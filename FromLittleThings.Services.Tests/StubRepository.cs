﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services.Tests
{
    public class StubRepository<T> : IRepository<T> where T : class
    {
        List<T> objectSet;

        public StubRepository()
        {
            objectSet = new List<T>();
        }

        public IQueryable<T> Fetch()
        {
            return objectSet.AsQueryable() ;
        }

        public virtual T FindByID(int id)
        {
            return objectSet.FirstOrDefault();
        }

        public IEnumerable<T> FindAll()
        {
            if (typeof(T) is IDeletable)
            {
                return objectSet.Where(i => !((IDeletable)i).IsDeleted).AsEnumerable();
            }
            else
            {
                return objectSet.AsEnumerable();
            }
        }

        public T Find(Func<T, bool> predicate)
        {
            if (typeof(T) is IDeletable)
            {
                return objectSet.Where(i => !((IDeletable)i).IsDeleted).SingleOrDefault<T>(predicate);
            }
            else
            {
                return objectSet.SingleOrDefault<T>(predicate);
            }

        }

        public void Add(T entity)
        {
            objectSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            objectSet.Remove(entity);
        }

        public void Attach(T entity)
        {
            if (!objectSet.Contains(entity))
                objectSet.Add(entity);

            return;
        }

        public void SaveChanges()
        {
            return;
        }
    }
}
