﻿using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace FromLittleThings.Services.Tests
{
    public class AchievementServiceTests
    {
        protected StubRepository<UserActivity> UserActivityRepository { get; set; }
        protected StubRepository<Achievement> AchievementRepository { get; set; }
        protected StubRepository<UserAchievement> UserAchievementRepository { get; set; }
        protected StubRepository<UserNotification> UserNotificationRepository { get; set; }
        protected AchievementService AchievementService { get; set; }

        public AchievementServiceTests()
        {
            UserActivityRepository = new StubRepository<UserActivity>();
            AchievementRepository = new StubRepository<Achievement>();
            UserAchievementRepository = new StubRepository<UserAchievement>();
            UserNotificationRepository = new StubRepository<UserNotification>();
            AchievementService = new AchievementService(AchievementRepository, UserAchievementRepository,
                UserActivityRepository, UserNotificationRepository);
        }


        [Fact]
        public void AchievementService_CalculateAchievements_NotificationCreated()
        {
            //ARRANGE
            var applicationUser = new ApplicationUser()
            {
                FirstName = "James",
                LastName = "Brown",
                Id = Guid.NewGuid().ToString()
            };

            PopulateAchievementAndActivity(applicationUser);

            //ACT
            AchievementService.CalculatAchievements(applicationUser);

            //ASSERT
            UserAchievementRepository.Fetch().Should().HaveCount(1);
            UserNotificationRepository.Fetch().Should().HaveCount(1);

        }

        [Fact]
        public void AchievementService_GetAchievements_HasAchievement()
        {
            //ARRANGE
            var applicationUser = new ApplicationUser()
            {
                FirstName = "James",
                LastName = "Brown",
                Id = Guid.NewGuid().ToString()
            };

            PopulateAchievementAndActivity(applicationUser);

            //ACT
            AchievementService.CalculatAchievements(applicationUser);
            var achievements = AchievementService.GetUserAchievements(applicationUser);

            //ASSERT
            achievements.Should().HaveCount(1);

        }


        private void PopulateAchievementAndActivity(ApplicationUser applicationUser)
        {
            var achievement = new Achievement()
             {
                 AchievementLevels = new List<AchievementLevel>() {
                { 
                    new AchievementLevel {
                    IsActive = true,
                    Name = "Level 1",
                    Points = 1,
                    TimelineText = "This is the timeline text"
                    }}
                },
                 IsActive = true,
             };
            achievement.AchievementLevels.First().Achievement = achievement;
            AchievementRepository.Add(achievement);


            UserActivityRepository.Add(new UserActivity()
            {
                IsActive = true,
                IsApproved = true,
                Points = 5,
                ActivityId = 1,
                ApplicationUser = applicationUser,
                UserId = applicationUser.Id,
                Activity = new Activity
                {
                    IsActive = true
                }
            });
        }


    }
}
