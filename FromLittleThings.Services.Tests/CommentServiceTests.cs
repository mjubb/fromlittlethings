﻿using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace FromLittleThings.Services.Tests
{
    public class CommentServiceTests
    {
        [Fact]
        public void CommentService_CreateComment_CommentExists()
        {
            //ARRANGE
            var commentRepository = new StubRepository<Comment>();
            var commentService = new CommentService(commentRepository);

            //ACT
            var comment = new Comment
            {
                CommentId = 1,
                Created = DateTime.Now,
                EntityRowId = Guid.NewGuid(),
                IsActive = true,
                IsDeleted = false,
                Text = "This is a comment"
            };

            commentService.CreateComment(comment);

            //ASSERT
            commentRepository.Fetch().Should().HaveCount(1);

        }


    }
}
