﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class BaseService
    {
        protected IRepository<Timeline> TimelineRepository { get; private set; }
        protected IRepository<ActivityGroup> ActivityGroupRepository { get; private set; }

        public BaseService(IRepository<Timeline> timelineRepository, IRepository<ActivityGroup> activityGroupRepository)
        {
            TimelineRepository = timelineRepository;
            ActivityGroupRepository = activityGroupRepository;
        }

    }
}
