﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class LeaderboardService
    {
        private IRepository<UserActivity> UserActivityRepository;


        public LeaderboardService(IRepository<UserActivity> userActivityRepository)
        {
            UserActivityRepository = userActivityRepository;

        }

        public Leaderboard GetLeaderboard(ApplicationUser ApplicationUser, int? itemsToTake, int? takeItemsBeforeId)
        {

            var userActivities = UserActivityRepository.Fetch().Where(ua => ua.Activity.IsActive && ua.IsApproved && ua.Activity.ActivityGroup.IsActive);

            var friends = ApplicationUser.GetFriends();

            var friendids = friends.Distinct().Select(f => f.Id);

            var items = (from ua in userActivities
                               where !ua.ApplicationUser.IsDeleted
                               && ua.ApplicationUser.IsActive
                               && friendids.Distinct().Contains(ua.UserId)
                               group ua by ua.ApplicationUser
                                   into g
                                   select new
                                   {
                                       ApplicationUser = g.Key,
                                       Points = g.Sum(ua => ua.Points) ?? 0
                                   }).ToList();




            var leaderboard = new Leaderboard();
            foreach(var friend in friends)
            {
                leaderboard.Items.Add(new LeaderboardItem
                {
                    ApplicationUser = friend,
                    Points = items.Any(f => f.ApplicationUser.Id == friend.Id) ? items.First(f => f.ApplicationUser.Id == friend.Id).Points : 0
                });
            }

            leaderboard.Items = leaderboard.Items.OrderByDescending(l => l.Points).Select(s => new LeaderboardItem
            {
                 ApplicationUser = s .ApplicationUser,
                 Points = s.Points,
                 Ranking = leaderboard.Items.IndexOf(s) + 1
            }).ToList();

            return leaderboard;

        }
    }

    public class Leaderboard
    {
        public IList<LeaderboardItem> Items { get; set; }

        public Leaderboard()
        {
            Items = new List<LeaderboardItem>();
        }
    }

    public class LeaderboardItem
    {
        public int Ranking { get; set; }
        public int Points { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }

}
