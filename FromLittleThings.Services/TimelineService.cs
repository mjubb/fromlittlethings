﻿using FromLittleThings.BlobService;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using FromLittleThings.Services.Dto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class TimelineService : BaseService
    {
        IBlobService TimelineImageService { get; set; }

        public TimelineService(IRepository<Timeline> timelineRepository, IRepository<ActivityGroup> activityGroupRepository, IBlobService timelineImageService)
            : base(timelineRepository, activityGroupRepository)
        {
            TimelineImageService = timelineImageService;
        }

        public AddUserPostResponse AddUserPost(AddUserPostRequest request)
        {
            var timeline = new Timeline();

            timeline.ApplicationUser = request.ApplicationUser;
            timeline.IsActive = true;
            timeline.Text = request.Text;
            timeline.Created = DateTime.UtcNow;

            if (request.File != null)
            {
                var fileName = Guid.NewGuid() + "." + Path.GetExtension(request.FileName);
                TimelineImageService.Put(new PutBlobRequest
                {
                    ContentType = "img/" + Path.GetExtension(fileName),
                    File = request.File,
                    FileName = request.FileName
                });
                timeline.ImageUrl = fileName;
            }

            TimelineRepository.Add(timeline);
            TimelineRepository.SaveChanges();

            var response = new AddUserPostResponse
            {
                Timeline = timeline
            };
            return response;
        }

        public void AddActivitiesToIimeline(ApplicationUser user, IList<UserActivity> userActivities)
        {
            var points = userActivities.Sum(ua => ua.Points);

            var activityGroupIds = userActivities.Select(u => u.Activity).Select(a => a.ActivityGroupId).Distinct();

            var activityGroups = ActivityGroupRepository.Fetch().ToList().Active().Where(ag => activityGroupIds.Contains(ag.ActivityGroupId));

            var text = GetActivitiesTimelineText(user.FirstName, points, activityGroups.Select(ag => ag.Name).ToArray());

            var timeline = new Timeline()
            {
                Created = DateTime.UtcNow,
                Text = text,
                EntityTypeId = (int)EntityTypes.Timeline,
                RowId = Guid.NewGuid(),
                UserId = user.Id,
                IsActive = true
            };

            TimelineRepository.Add(timeline);

            TimelineRepository.SaveChanges();

        }

        public void AddAchievemensToTimeline(ApplicationUser user, IList<UserAchievement> userAchievements)
        {
            foreach (var userAchievement in userAchievements)
            {
                var text = user.FirstName + " has added the " + userAchievement.AchievementLevel.Name + " to their collection.";

                var timeline = new Timeline()
                {
                    Created = DateTime.UtcNow,
                    Text = text,
                    EntityTypeId = (int)EntityTypes.Timeline,
                    RowId = Guid.NewGuid(),
                    UserId = user.Id,
                    IsActive = true
                };

                TimelineRepository.Add(timeline);

            }

            TimelineRepository.SaveChanges();

        }


        private string GetActivitiesTimelineText(string firstName, int? points, string[] groups)
        {
            var text = "";
            if (points.HasValue)
            {
                var pointsText = points.Value > 1 ? "points" : "points";
                text = "{0} just earned {1} {2} by doing some little things in {3}.";
                text = string.Format(text, firstName, points, pointsText, string.Join(", ", groups));
            }
            else
            {
                text = "{0} just did some great work in {1}.";
                text = string.Format(text, firstName, string.Join(", ", groups));
            }

            return text;
        }

        public IList<Timeline> GetTimeline(ApplicationUser ApplicationUser, int? itemsToTake, int? takeItemsBeforeId)
        {


            var friends = ApplicationUser.GetFriends();

            var friendids = friends.Distinct().Select(f => f.Id);

            var timeline = (from n in TimelineRepository.Fetch() //.Active()
                            where !n.ApplicationUser.IsDeleted
                                //&& n.ApplicationUser.IsActive
                            && friendids.Distinct().Contains(n.UserId)
                            && ((!takeItemsBeforeId.HasValue) || (n.TimelineId < takeItemsBeforeId.Value))
                            orderby n.TimelineId descending
                            select n);

            var snack = new List<Timeline>();
            if (itemsToTake.HasValue)
            {
                snack = timeline.Take(itemsToTake.Value).ToList();
            }
            else
            {
                snack = timeline.ToList();
            }

            ChangeTimelineProperNouns(snack, ApplicationUser);

            return snack.ToList();

        }



        private void ChangeTimelineProperNouns(IList<Timeline> timeline, ApplicationUser ApplicationUser)
        {

            foreach (var item in timeline.Where(i => i.UserId == ApplicationUser.Id))
            {
                item.Text = item.Text.Replace(ApplicationUser.FullName + " has", "You have");
                item.Text = item.Text.Replace(ApplicationUser.FullName + " is", "You are");
                if (item.Text.StartsWith(ApplicationUser.FullName))
                {
                    item.Text = item.Text.Replace(ApplicationUser.FullName, "You");
                }
                else
                {
                    item.Text = item.Text.Replace(ApplicationUser.FullName, "you");
                }
            }
        }

        public Timeline GetTimeline(int itemId)
        {

            var timeline = TimelineRepository.Find(t => t.TimelineId == itemId);
            return timeline;
        }
    }

}
