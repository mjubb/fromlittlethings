﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class ApplicationUserService
    {
        private IRepository<ApplicationUser> ApplicationUserRepository;
        private IRepository<UserActivity> UserActivityRepository;

        public ApplicationUserService(IRepository<ApplicationUser> applicationUserRespository, IRepository<UserActivity> userActivityRepository)
        {
            ApplicationUserRepository = applicationUserRespository;
            UserActivityRepository = userActivityRepository;
        }

        public int GetTotalPoints(ApplicationUser user)
        {
            var currentPoints = (from ua in UserActivityRepository.Fetch()
                                 where ua.UserId == user.Id
                                 && ua.IsActive && ua.IsApproved && !ua.IsDeleted
                                 && ua.Activity.IsActive && !ua.IsDeleted
                                 && ua.Points.HasValue
                                 select ua.Points.Value)
                                .DefaultIfEmpty(0)
                                .Sum();

            return currentPoints;
        }

        public Dictionary<DateTime, int> GetPreviousPoints(ApplicationUser user, int daysPrevious)
        {
            if (daysPrevious < 0)
                daysPrevious = 0;

            var pointsByDay = new Dictionary<DateTime, int>();
            for(int i = 0; i < daysPrevious; i++)
            {
                var date = DateTime.UtcNow.Date.AddDays(-i);
                var nextDate = date.AddDays(1);
                var points = (from ua in UserActivityRepository.Fetch()
                                     where ua.UserId == user.Id
                                     && ua.IsActive && ua.IsApproved && !ua.IsDeleted
                                     && ua.Activity.IsActive && !ua.IsDeleted
                                     && ua.ActivityDate >= date 
                                     && ua.ActivityDate < nextDate
                                     && ua.Points.HasValue
                                     select ua.Points.Value)
                                     .DefaultIfEmpty(0)
                                     .Sum();

                pointsByDay.Add(date, points);
            }
            return pointsByDay;
        }

    }

}
