﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class AchievementService
    {
        protected IRepository<Achievement> AchievementRepository { get; private set; }
        protected IRepository<UserAchievement> UserAchievementRepository { get; private set; }
        protected IRepository<UserActivity> UserActivityRepository { get; private set; }
        protected IRepository<UserNotification> UserNotificationRepository { get; private set; }

        public AchievementService(IRepository<Achievement> achievementRepository, IRepository<UserAchievement> userAchievementRepository, IRepository<UserActivity> userActivityRepository, IRepository<UserNotification> userNotificationRepository)
        {
            AchievementRepository = achievementRepository;
            UserAchievementRepository = userAchievementRepository;
            UserActivityRepository = userActivityRepository;
            UserNotificationRepository = userNotificationRepository;
        }

        public IList<UserAchievement> CalculatAchievements(ApplicationUser user)
        {
            var currentAchievementLevels = UserAchievementRepository.Fetch().Where(ua => ua.UserId == user.Id).Select(ua => ua.AchievementLevel).ToList();

            var currentPoints = (from ua in UserActivityRepository.Fetch()
                                 where ua.UserId == user.Id
                                 && ua.IsActive && ua.IsApproved && !ua.IsDeleted
                                 && ua.Activity.IsActive && !ua.IsDeleted
                                 && ua.Points.HasValue
                                 select ua).Sum(ua => ua.Points.Value);

            var validAchievementLevels = AchievementRepository.Fetch().Active().SelectMany(ua => ua.AchievementLevels).Active().Where(al => al.Points <= currentPoints).ToList();

            var addedAchievementLevels = new List<UserAchievement>();
            foreach (var achievementLevel in validAchievementLevels)
            {
                if (!currentAchievementLevels.Any(al => al.AchievementLevelId == achievementLevel.AchievementLevelId))
                {
                    var ua = new UserAchievement()
                    {
                        AchievementLevel = achievementLevel,
                        UserId = user.Id,
                        Points = achievementLevel.Points,
                        Created = DateTime.UtcNow,
                        IsActive = true,
                        EntityTypeId = (int)EntityTypes.UserAchievement,
                        IsApproved = true,
                        IsDeleted = false,
                        RowId = Guid.NewGuid(),
                    };
                    UserAchievementRepository.Add(ua);
                    addedAchievementLevels.Add(ua);

                    var userNotification = new UserNotification
                    {
                        ApplicationUser = user,
                        UserId = user.Id,
                        Created = DateTime.UtcNow,
                        IsActive = true,
                        Text = string.Format("Congratulations! You have earned the {0}!", achievementLevel.Name)
                    };

                    UserNotificationRepository.Add(userNotification);
                }
            }

            UserAchievementRepository.SaveChanges();

            return addedAchievementLevels;
        }

        public IList<UserAchievement> GetUserAchievements(ApplicationUser applicationUser)
        {
            return (from ua in UserAchievementRepository.Fetch().Active()
                   where ua.AchievementLevel.IsActive && !ua.AchievementLevel.IsDeleted
                   && ua.UserId == applicationUser.Id
                   && ua.AchievementLevel.Achievement.IsActive && !ua.AchievementLevel.Achievement.IsDeleted
                   select ua).ToList();

        }

    }

}
