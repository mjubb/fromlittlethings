﻿using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services.Dto
{
    public class AddUserPostResponse
    {
        public Timeline Timeline { get; set; }
    }
}
