﻿using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services.Dto
{
    public class AddUserPostRequest
    {
        public ApplicationUser ApplicationUser { get; set; }
        public string Text { get; set; }
        public FileStream File { get; set; }
        public string FileName { get; set; }
    }
}
