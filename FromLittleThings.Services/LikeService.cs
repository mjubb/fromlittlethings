﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class LikeService
    {
        protected IRepository<Like> LikeRepository { get; private set; }

        public LikeService(IRepository<Like> likeRepository)
        {
            LikeRepository = likeRepository;
        }


        public IList<Like> GetLikes(IActable entity)
        {
            return LikeRepository.Fetch().ForRow(entity.RowId).ToList();
        }

        public IList<Like> GetActiveLikes(IActable entity)
        {
            return LikeRepository.Fetch().Active().ForRow(entity.RowId).ToList();
        }

        public Like GetLike(int likeId)
        {
            return LikeRepository.FindByID(likeId);
        }

        public Like CreateLike(Like like)
        {
            LikeRepository.Add(like);
            LikeRepository.SaveChanges();
            return like;
        }

        public Like UpdateLike(Like like)
        {
            LikeRepository.SaveChanges();
            return like;
        }

        public void DeleteLike(Like like)
        {
            LikeRepository.Delete(like);
            LikeRepository.SaveChanges();
            return;
        }

    

    }
}
