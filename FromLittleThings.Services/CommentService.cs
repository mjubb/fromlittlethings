﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class CommentService
    {
        protected IRepository<Comment> CommentRepository { get; private set; }

        public CommentService(IRepository<Comment> commentRepository)
        {
            CommentRepository = commentRepository;
        }


        public IList<Comment> GetComments(IActable entity)
        {
            return CommentRepository.Fetch().ForRow(entity.RowId).ToList();
        }

        public IList<Comment> GetActiveComments(IActable entity)
        {
            return CommentRepository.Fetch().Active().ForRow(entity.RowId).ToList();
        }

        public Comment GetComment(int commentId)
        {
            return CommentRepository.FindByID(commentId);
        }

        public Comment CreateComment(Comment comment)
        {
            CommentRepository.Add(comment);
            CommentRepository.SaveChanges();
            return comment;
        }

        public Comment UpdateComment(Comment comment)
        {
            CommentRepository.SaveChanges();
            return comment;
        }

        public void DeleteComment(Comment comment)
        {
            CommentRepository.Delete(comment);
            CommentRepository.SaveChanges();
            return;
        }

    

    }
}
