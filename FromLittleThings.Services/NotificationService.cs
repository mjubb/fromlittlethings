﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class NotificationService
    {
        private IRepository<UserNotification> UserNotificationRepository;


        public NotificationService(IRepository<UserNotification> userNotificationRepository)
        {
            UserNotificationRepository = userNotificationRepository;

        }

        public IList<UserNotification> GetUnreadNotifications(ApplicationUser applicationUser)
        {

            var userActivities = UserNotificationRepository.Fetch().Where(ua => ua.ApplicationUser.Id == applicationUser.Id && ua.IsActive && !ua.IsRead && !ua.IsSent).ToList();

            foreach (var ua in userActivities)
            {
                ua.IsSent = true;
                ua.SentDate = DateTime.UtcNow;
            }

            UserNotificationRepository.SaveChanges();

            return userActivities.ToList();

        }
    }
}
