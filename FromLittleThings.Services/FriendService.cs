﻿using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Services
{
    public class FriendService
    {
        private IRepository<ApplicationUser> ApplicationUserRepository;
        private IRepository<Friend> FriendRepository;
        private IRepository<UserNotification> UserNotificationRepository;

        public FriendService(IRepository<ApplicationUser> ApplicationUserRespository, IRepository<Friend> friendRepository, IRepository<UserNotification> userNotificationRepository)
        {
            ApplicationUserRepository = ApplicationUserRespository;
            FriendRepository = friendRepository;
            UserNotificationRepository = userNotificationRepository;
        }

        public List<ApplicationUser> GetFriendApplicationUsers(ApplicationUser user)
        {
            var received = FriendRepository.FindAll().Where(u => u.UserId == user.Id).Active().Select(f => f.FriendApplicationUser).ToList();
            var sent = FriendRepository.FindAll().Where(u => u.FriendUserId == user.Id).Active().Select(f => f.ApplicationUser).ToList();

            return received.Union(sent).Distinct().ToList();
        }


        public List<Friend> GetFriends(ApplicationUser user)
        {
            var received = FriendRepository.FindAll().Where(u => u.UserId == user.Id).Active().ToList();
            var sent = FriendRepository.FindAll().Where(u => u.FriendUserId == user.Id).Active().ToList();

            return received.Union(sent).Distinct().ToList();
        }

        public void DeleteFriend(ApplicationUser user, int id)
        {
            var friend = FriendRepository.Find(f => f.FriendId == id);

            if (friend == null)
                throw new ApplicationException("The friend could not be found.");

            if ((friend.UserId == user.Id) || (friend.FriendUserId == user.Id))
            {
                FriendRepository.Delete(friend);
                FriendRepository.SaveChanges();
            }

            return;
        }

        public void AddFriend(ApplicationUser user, string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                throw new ApplicationException("The email is invalid.");

            email = email.ToString().ToLower();

            var friendApplicationUser = ApplicationUserRepository.Find(u => u.UserName.ToLower() == email);

            if (friendApplicationUser == null)
            {
                throw new ApplicationException("We don't have a user with that email registered.");
            }

            if (user.GetFriends(false).Any(f => f.Id == friendApplicationUser.Id))
            {
                throw new ApplicationException(string.Format("{0} is already your friend.", friendApplicationUser.FullName));
            }

            var friend = new Friend()
            {
                Created = DateTime.UtcNow,
                IsActive = true,
                IsDeleted = false,
                UserId = user.Id,
                FriendUserId = friendApplicationUser.Id
            };

            FriendRepository.Add(friend);

            FriendRepository.SaveChanges();


            Task.Factory.StartNew(() =>
            {
                var un = new UserNotification
                {
                    ApplicationUser = friendApplicationUser,
                    Text = user.FullName + " just added you as a friend!",
                    IsActive = true,
                    Created = DateTime.UtcNow,
                    UserId = friendApplicationUser.Id,
                };
                UserNotificationRepository.Add(un);
                UserNotificationRepository.SaveChanges();
            });


        }

    }

}
