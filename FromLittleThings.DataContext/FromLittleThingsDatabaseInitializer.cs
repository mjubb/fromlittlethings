﻿using FromLittleThings.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.DataContext
{
    //public class LittleThingsDatabaseInitializer : DropCreateDatabaseAlways<LittleThingsContext>
    public class LittleThingsDatabaseInitializer : DropCreateDatabaseIfModelChanges<LittleThingsContext>
    {
        protected override void Seed(LittleThingsContext context)
        {
            var ag = new List<ActivityGroup>();

            ag.Add(new ActivityGroup()
            {
                Name = "The Home",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Used the stairs instead of the elevator.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Took a fast (under 3 minutes!) shower.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Washed the clothes with an evironmentally friendly detergent.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Used the washing line instead of the clothes dryer.", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Didn't use air conditioning or a heater in the house.", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                }
            });


            ag.Add(new ActivityGroup()
            {
                Name = "Recycling",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Seperated the recycling from normal rubbish.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Took a battery to a battery recycler.", Points = 3, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Delivered old electronics and mobile phone to a recycling depot.", Points = 5, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false},
                    new Activity { Name = "Donated used furniture, clothes or electronics to a charity.", Points = 3, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false},
                
                
                }
            });


            ag.Add(new ActivityGroup()
            {
                Name = "Shopping",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Took my own grocery bags to the shop .", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Took my own container to buy take away food or coffee.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Didn't buy fruit and veg pre-packaged in plastic or styrofoam.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Bought organic food, including free range eggs and meat.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Bought an item second hand instead of new.", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Bought recycled toilet paper.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Chose biodegradable cleaning products.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Bought recycled printer or writing paper.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Bought Fair Trade approved chocolate, coffee or tea.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Researched a brand to check it's ethical rating before buying.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Shopped in an organic shop or farmers market instead of a supermarket.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Bought a product free of animal materials.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Checked a products' carbon print before buying.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                    new Activity { Name = "Bought nothing today.", Points = 3, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false} ,
                }
            });


            ag.Add(new ActivityGroup()
            {
                Name = "Eating",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Ate vegetarian all day.", Points = 5, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Chose fair trade.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Supported restaurants that serve sustainable fish/fairtrade.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Prepared meal with leftovers, instead of throwing them out .", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                }
            });

            ag.Add(new ActivityGroup()
            {
                Name = "Getting Around",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Walked or biked to work/school.", Points = 4, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Took public transport to work/school.", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Took a train instead of a flight .", Points = 5, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                }
            });

            ag.Add(new ActivityGroup()
            {
                Name = "The neighbourhood",
                IsActive = true,
                IsDeleted = false,
                Description = "",
                Activities = new List<Activity>()
                {
                    new Activity { Name = "Picked up garbage from the ground and put it in the bin/recycling.", Points = 1, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Follow or start a green campaign.", Points = 2, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                    new Activity { Name = "Organised a neighbourhood clean-up .", Points = 10, Description = "", Created = DateTime.Now, IsActive = true, IsDeleted = false}, 
                }
            });

            foreach (var activityGroup in ag)
                context.ActivityGroups.Add(activityGroup);

            //var passwordHash = new PasswordHasher();
            //string password = passwordHash.HashPassword("mike123");

            var user = new ApplicationUser()
            {
                Created = DateTime.UtcNow,
                UserName = "michael.jubb@gmail.com",
                Email = "michael.jubb@gmail.com",
                FirstName = "Michael",
                LastName = "Jubb",
                IsActive = true,
                IsDeleted = false,
                //      PasswordHash = password,
                Id = Guid.NewGuid().ToString()
            };

            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);
            userManager.Create(user, "mike123");

            var user2 = new ApplicationUser()
            {
                Created = DateTime.UtcNow,
                UserName = "fernandalorza@gmail.com",
                Email = "fernandalorza@gmail.com",
                FirstName = "Fernanda",
                LastName = "Lorza",
                IsActive = true,
                IsDeleted = false,
                Id = Guid.NewGuid().ToString()
            };
            userManager.Create(user2, "fernanda123");

            var achievement = new Achievement
            {
                Created = DateTime.UtcNow,
                Description = "Helping the environment is helping the animals.",
                Name = "Happy Animals",
                IsActive = true,
                IsDeleted = false,
                AchievementLevels = new List<AchievementLevel>()
                {
                    new AchievementLevel{ Created = DateTime.UtcNow,
                        Description = "The little monkey is found in Borneo and is very cute!",
                        Name ="Little Monkey",
                        Points = 10,
                        TimelineText = "{0} has helped the Little Monkey which now has a cleaner environment to raise its little Little Monkeys!",
                        IsActive = true,
                        IsDeleted = false,
                        ImageUrl = "https://s-media-cache-ak0.pinimg.com/736x/6b/c5/37/6bc537a241ffc746acb7d2180d2253d8.jpg"
                    },

                    new AchievementLevel{ Created = DateTime.UtcNow,
                        Description = "The African white rhinoceros is the most endangered animal on earth",
                        Name ="White Rhinoceros",
                        Points = 30,
                        TimelineText = "{0} has helped the African White rhinoceros, the most endangered animal on earth.",
                        IsActive = true,
                        IsDeleted = false,
                        ImageUrl = "http://www.ebsqart.com/Art/ACEO/Mixed-media/687088/650/650/Curious-Little-Rhino.jpg"
                    },

                    new AchievementLevel{ Created = DateTime.UtcNow,
                        Description = "The Clownfish, also knows as XXXX or nemo, is abundant in many areas. However....",
                        Name ="Clownfish",
                        Points = 90,
                        TimelineText = "{0} has saved Clownfish (better known as Nemo!), the cutest of all fish.",
                        IsActive = true,
                        IsDeleted = false,
                        ImageUrl = "https://furryfurynailart.files.wordpress.com/2010/09/nemo.jpg"
                    }

                }
            };

            context.Achievements.Add(achievement);

            if (false)
            {
                for (var i = 0; i < 150; i++)
                {
                    var timeline = new Timeline
                    {
                        ApplicationUser = user,
                        Created = DateTime.Now,
                        EntityTypeId = 1,
                        IsActive = true,
                        RowId = Guid.NewGuid(),
                        Text = "This is the text of timeline number " + i.ToString(),
                    };
                    context.Timelines.Add(timeline);
                }
            }

            base.Seed(context);
        }
    }

}
