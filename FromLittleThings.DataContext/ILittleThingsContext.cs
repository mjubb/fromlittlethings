﻿using System;
namespace FromLittleThings.DataContext
{
    public interface ILittleThingsContext
    {
        System.Data.Entity.DbSet<FromLittleThings.Model.AchievementLevel> AchievementLevels { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.Achievement> Achievements { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.Activity> Activities { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.ActivityGroup> ActivityGroups { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.Timeline> Timelines { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.UserAchievement> UserAchievements { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.UserActivity> UserActivities { get; set; }
        System.Data.Entity.DbSet<FromLittleThings.Model.Comment> Comments { get; set; }
    }   
}
