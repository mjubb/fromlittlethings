﻿using FromLittleThings.Model;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.DataContext
{
    public class LittleThingsContext : IdentityDbContext<ApplicationUser>, FromLittleThings.DataContext.ILittleThingsContext
    {
        public LittleThingsContext()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer(new LittleThingsDatabaseInitializer());
        }

        public DbSet<Timeline> Timelines { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<UserActivity> UserActivities { get; set; }
        public DbSet<ActivityGroup> ActivityGroups { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<AchievementLevel> AchievementLevels { get; set; }
        public DbSet<UserAchievement> UserAchievements { get; set; }
        public DbSet<UserNotification> UserNotifications { get; set; }          
        public DbSet<Like> Likes { get; set; }
        public DbSet<Comment> Comments { get; set; }

//        public DbSet<Invitation> Invitations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                   .HasMany(p => p.FriendsSent)
                   .WithOptional()
                   .Map(conf => conf.MapKey("UserId"))
                   .WillCascadeOnDelete(false);

            modelBuilder.Entity<ApplicationUser>()
                   .HasMany(p => p.FriendsReceived)
                   .WithOptional()
                   .Map(conf => conf.MapKey("FriendUserId"))
                   .WillCascadeOnDelete(false);

            modelBuilder.Entity<Friend>()
                    .HasRequired(f => f.FriendApplicationUser)
                    .WithMany()
                    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Friend>()
                    .HasRequired(f => f.ApplicationUser)
                    .WithMany()
                    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Invitation>()
            //        .HasRequired(f => f.ApplicationUser)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Invitation>()
            //        .HasOptional(f => f.InvitedApplicationUser)
            //        .WithMany()
            //        .WillCascadeOnDelete(false);

        }
    }
}
