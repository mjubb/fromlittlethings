﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.BlobService
{
    public interface IBlobService
    {
        GetBlobResponse Get(string fileName);
        void Put(PutBlobRequest request);
    }
}
