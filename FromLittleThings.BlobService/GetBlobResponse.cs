﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.BlobService
{
    public class GetBlobResponse
    {
        public MemoryStream File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}
