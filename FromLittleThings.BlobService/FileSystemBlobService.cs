﻿using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.BlobService
{
    public class FileSystemBlobService : BlobService, IBlobService
    {
        protected IFileSystemBlobServiceConfiguration Configuration { get; private set; }

        public FileSystemBlobService(IFileSystemBlobServiceConfiguration configuration)
        {
            Configuration = configuration;
        }

        public GetBlobResponse Get(string fileName)
        {
  
            var fullPath = Path.Combine(Configuration.BlobStoragePath, fileName);

            if (!File.Exists(fullPath))
                throw new Exception("File does not exist.");

            var copy = new MemoryStream();
            using (var fs = File.OpenRead(fullPath))
            {
                fs.CopyTo(copy);
            }

            var response = new GetBlobResponse
            {
                FileName = fileName,
                ContentType = "",
                File = copy
            };

            return response;
        }

        public void Put(PutBlobRequest request)
        {
            if (!Directory.Exists(Configuration.BlobStoragePath))
                Directory.CreateDirectory(Configuration.BlobStoragePath);

            var fullPath = Path.Combine(Configuration.BlobStoragePath, request.FileName);

            using (var fs = File.Create(fullPath))
            {
                request.File.Seek(0, SeekOrigin.Begin);
                request.File.CopyTo(fs);
            }

            return;
        }
    }
}
