﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace FromLittleThings.BlobService
{
    public class PutBlobRequest
    {
        public Stream File { get; set; }
        public string FileName { get; set; }
        public string ContentType { get; set; }
    }
}
