﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.BlobService
{
    public interface IFileSystemBlobServiceConfiguration
    {
        string BlobStoragePath { get; }
    }
}
