﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.BlobService
{
    public class AzureBlobService : BlobService, IBlobService
    {
        protected IAzureBlobServiceConfiguration Configuration { get; private set; }

        public AzureBlobService(IAzureBlobServiceConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void Put(PutBlobRequest request)
        {
            var blobContainer = GetBlobContainer();
            var blob = blobContainer.GetBlockBlobReference(request.FileName);

            blob.Properties.ContentType = request.ContentType;
            request.File.Position = 0;
            blob.UploadFromStream(request.File);

            return;
        }

        public GetBlobResponse Get(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                var container = GetBlobContainer();
                var blob = container.GetBlockBlobReference(fileName);

                var ms = new MemoryStream();
                blob.DownloadToStream(ms);
                //await blob.DownloadToStreamAsync(ms);

                var download = new GetBlobResponse
                {
                    File = ms,
                    FileName = fileName,
                    ContentType = blob.Properties.ContentType
                };

                return download;
            }

            return null;
        }

        private CloudBlobContainer GetBlobContainer()
        {
            var blobStorageConnectionString = Configuration.BlobStorageConnectionString;
            var blobStorageContainerName = Configuration.BlobStorageContainerName;

            var blobStorageAccount = CloudStorageAccount.Parse(blobStorageConnectionString);
            var blobClient = blobStorageAccount.CreateCloudBlobClient();
            return blobClient.GetContainerReference(blobStorageContainerName);
        }
    }
}
