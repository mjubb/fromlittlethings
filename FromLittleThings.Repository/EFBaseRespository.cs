﻿
using FromLittleThings.DataContext;
using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Repository
{
    public class EFBaseRepository<T> : IRepository<T> where T : class
    {
        protected FromLittleThings.DataContext.LittleThingsContext dataContext;
        protected DbSet<T> objectSet;

        public EFBaseRepository(ILittleThingsContext context)
        {
            dataContext = (FromLittleThings.DataContext.LittleThingsContext)context;
            objectSet = dataContext.Set<T>();
        }

        #region IBaseRepository<T> Members

        public IQueryable<T> Fetch()
        {
    return objectSet;
        }

        public virtual T FindByID(int id)
        {

            var key = new EntityKey("LittleThingsContext." + EntityName, typeof(T).Name + "Id", id);

            T ret = null;

            try
            {

                ret = (T)((IObjectContextAdapter)dataContext).ObjectContext.GetObjectByKey(key);
            }
            catch
            {
                try
                {
                    key = new EntityKey("LittleThingsContext." + EntityName, typeof(T).Name + "ID", id);
                    ret = (T)((IObjectContextAdapter)dataContext).ObjectContext.GetObjectByKey(key);
                }
                catch { }
            }

            //if the item is soft deleteable, and the flag is set, return null
            if ((ret != null) && (ret is IDeletable) && (((IDeletable)ret).IsDeleted))
            {
                ret = null;
            }

            if (ret != null)
            {
                ((IObjectContextAdapter)dataContext).ObjectContext.Refresh(RefreshMode.StoreWins, ret);
            }

            return ret;
        }

        private string EntityName
        {
            get
            {
                String name = typeof(T).Name;
                if (name.Substring(name.Length - 1, 1).ToLower() == "y")
                    return name.Remove(name.Length - 1, 1) + "ies";
                else if (name.Substring(name.Length - 1, 1).ToLower() == "s")
                    return name + "es";
                else
                    return name + "s";
            }
        }

        public IEnumerable<T> FindAll()
        {
            if (typeof(T) is IDeletable)
            {
                return objectSet.Where(i => !((IDeletable)i).IsDeleted).AsEnumerable();
            }
            else
            {
                return objectSet.AsEnumerable();
            }
        }

        public T Find(Func<T, bool> predicate)
        {
            if (typeof(T) is IDeletable)
            {
                return objectSet.Where(i => !((IDeletable)i).IsDeleted).SingleOrDefault<T>(predicate);
            }
            else
            {
                return objectSet.SingleOrDefault<T>(predicate);
            }

        }

        public void Add(T entity)
        {
            objectSet.Add(entity);
        }

        public virtual void Delete(T entity)
        {
            objectSet.Remove(entity);
        }

        public void Attach(T entity)
        {
            objectSet.Attach(entity);
        }

        public void SaveChanges()
        {
            try
            {
                dataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        #endregion
    }
}
