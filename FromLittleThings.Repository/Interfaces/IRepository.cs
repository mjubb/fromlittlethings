﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Repository
{
    public interface IRepository<T> where T : class
    {
        IQueryable<T> Fetch();
        IEnumerable<T> FindAll();
        T FindByID(int ID);
        T Find(Func<T, bool> predicate);
        void Add(T entity);
        void Delete(T entity);
        void Attach(T entity);
        void SaveChanges();
    }
}
