﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity.ModelConfiguration;
using System.ComponentModel.DataAnnotations.Schema;

namespace FromLittleThings.Model
{
    public class ApplicationUser : IdentityUser, IActive, IDeletable, ICreated
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfileImageUrl { get; set; }
        public string ProfileImageThumbUrl { get; set; }
        public DateTimeOffset Created { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<UserActivity> UserActivities { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<Timeline> Timeline { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<Like> Likes { get; set; }

        [ForeignKey("UserId")]
        public virtual ICollection<Friend> FriendsSent { get; set; }

        [ForeignKey("FriendUserId")]
        public virtual ICollection<Friend> FriendsReceived { get; set; }

        [NotMapped]
        public string FullName
        {
            get
            {
                return (FirstName + " " + LastName).Trim();
            }
        }

        public IList<ApplicationUser> GetFriends(bool includeMe = true)
        {
            var friends = new List<ApplicationUser>(); ;
            friends.AddRange(FriendsSent.Active().Select(f => f.FriendApplicationUser).ToList());
            friends.AddRange(FriendsReceived.Active().Select(f => f.ApplicationUser).ToList());
            if (includeMe)
                friends.Add(this);
            return friends;
        }

        public ApplicationUser()
        {
            FriendsSent = new List<Friend>();
            FriendsReceived = new List<Friend>();
            Timeline = new List<Timeline>();
            UserActivities = new List<UserActivity>();
            Created = DateTime.UtcNow;
            FirstName = "";
            LastName = "";
            ProfileImageUrl = "";
            ProfileImageThumbUrl = "";
            IsActive = true;
        }
    }

}
    