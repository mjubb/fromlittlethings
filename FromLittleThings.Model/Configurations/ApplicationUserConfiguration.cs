﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model.Configurations
{

    public class ApplicationUserConfiguration : EntityTypeConfiguration<ApplicationUser>
    {
        public ApplicationUserConfiguration()
        {
            Property(u => u.UserName).IsRequired();
            HasMany<IdentityUserRole>(u => u.Roles);
        }
    }

    public class IdentityRoleConfiguration : EntityTypeConfiguration<IdentityRole>
    {
        public IdentityRoleConfiguration()
        {
            Property(r => r.Name).IsRequired();
        }
    }

    public class IdentityUserClaimConfiguration : EntityTypeConfiguration<IdentityUserClaim>
    {
        public IdentityUserClaimConfiguration()
        {
            //HasRequired(u => u..User);
        }


    }
    public class IdentityUserLoginConfiguration : EntityTypeConfiguration<IdentityUserLogin>
    {
        public IdentityUserLoginConfiguration()
        {
            HasKey(l => new
            {
                l.UserId,
                l.LoginProvider,
                l.ProviderKey
            });
           // HasRequired<IdentityUser>(u => u.UserId);
        }
    }

    public class IdentityUserRoleConfiguration : EntityTypeConfiguration<IdentityUserRole>
    {
        public IdentityUserRoleConfiguration()
        {
            HasKey(r => new
            {
                r.UserId,
                r.RoleId
            });
        }
    }



}
