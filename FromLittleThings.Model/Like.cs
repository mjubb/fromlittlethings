﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class Like : IActive, IAction, ICreated, IDeletable
    {
        public int LikeId { get; set; }
        public string UserId { get; set; }
        public Guid EntityRowId { get; set; }
        public int EntityTypeId { get; set; }
        public DateTimeOffset Created { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
