﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public static class ICreatedExtensions
    {
        public static IEnumerable<T> CreatedBetween<T>(this ICollection<T> collection, DateTime fromDate, DateTime endDate) where T : ICreated
        {
            return collection.Where(u => u.Created >= fromDate && u.Created <= endDate);
        }

        public static IEnumerable<T> CreatedBetween<T>(this IEnumerable<T> collection, DateTime fromDate, DateTime endDate) where T : ICreated
        {
            return collection.Where(u => u.Created >= fromDate && u.Created <= endDate);
        }

        public static IQueryable<T> CreatedBetween<T>(this IQueryable<T> collection, DateTime fromDate, DateTime endDate) where T : ICreated
        {
            return ((IQueryable<ICreated>)collection).Where(u => u.Created >= fromDate && u.Created <= endDate).Cast<T>();
        }

    }
}
