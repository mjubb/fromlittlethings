﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public static class IActionExtensions
    {
        public static IList<T> ForRow<T>(this ICollection<T> collection, Guid rowId) where T : IAction
        {
            return collection.Where(u => u.EntityRowId == rowId).ToList();
        }

        public static IQueryable<T> ForRow<T>(this IQueryable<T> collection, Guid rowId) where T : IAction
        {
            return ((IQueryable<IAction>)collection).Where(u => u.EntityRowId == rowId).Cast<T>();
        }

        public static IEnumerable<T> ForRow<T>(this IEnumerable<T> collection, Guid rowId) where T : IAction
        {
            return collection.Where(u => u.EntityRowId == rowId);
        }

    }
}
