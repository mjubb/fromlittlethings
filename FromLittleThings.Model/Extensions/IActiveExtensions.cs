﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public static class IActiveExtensions
    {
        public static IList<T> Active<T>(this ICollection<T> activeCollection) where T : IActive
        {
            return activeCollection.Where(u => !u.IsDeleted && u.IsActive).ToList();
        }

        public static IQueryable<T> Active<T>(this IQueryable<T> activeCollection) where T : IActive
        {
            return ((IQueryable<IActive>)activeCollection).Where(c => c.IsActive && !c.IsDeleted).Cast<T>();
        }

        public static IEnumerable<T> Active<T>(this IEnumerable<T> activeCollection) where T : IActive
        {
            return activeCollection.Where(u => !u.IsDeleted && u.IsActive);
        }

    }
}
