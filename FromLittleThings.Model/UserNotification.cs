﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class UserNotification : ICreated, IActive, IDeletable, IApplicationUsered
    {
        public int UserNotificationId { get; set; }
        public string UserId { get; set; }
        public string NotificationCategory { get; set; }
        public string NotificationType { get; set; }
        public string NotificationLink { get; set; }
        public string Text { get; set; }
        public string Sound { get; set; }
        public string Image { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset? SentDate { get; set; }
        public bool IsSent { get; set; }
        public bool IsRead { get; set; }
        public DateTimeOffset? ReadDate { get; set; }
        public string ErrorInformation { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }
    }
}
