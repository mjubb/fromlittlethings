﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class UserActivity : ICreated, IActive, IDeletable, IApplicationUsered
    {
        public int UserActivityId { get; set; }
        public string UserId { get; set; }
        public int ActivityId { get; set; }
        public int? Points { get; set; }
        public bool IsApproved { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public Guid RowId { get; set; }
        public int EntityTypeId { get; set; }
        public DateTimeOffset ActivityDate { get; set; }
        public DateTimeOffset Created { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual Activity Activity { get; set; }

    }
}
