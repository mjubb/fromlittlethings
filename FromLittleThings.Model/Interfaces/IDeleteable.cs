﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public interface IDeletable
    {
        bool IsDeleted { get; set; }
        DateTimeOffset? DeletedDate { get; set; }
    }
}
