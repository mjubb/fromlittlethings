﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public interface IActable
    {
        Guid RowId { get; set; }
        int EntityTypeId { get; }
    }
}
