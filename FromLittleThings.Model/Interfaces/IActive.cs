﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public interface IActive
    {
        bool IsActive { get; set; }
        bool IsDeleted { get; set; }
    }
}
