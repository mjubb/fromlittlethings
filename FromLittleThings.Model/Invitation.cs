﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class Invitation : IActive, ICreated, IDeletable
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int InvitationId { get; set; }
        public Guid Guid { get; set; }
        public string UserId { get; set; }
        public string InvitedEmail { get; set; }
        public string InvitedUserId { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset? Accepted { get;set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [ForeignKey("InvitedUserId")]
        public virtual ApplicationUser InvitedApplicationUser { get; set; }
    
    }
}
