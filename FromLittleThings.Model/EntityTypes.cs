﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public enum EntityTypes
    {
        Timeline = 1,
        Comment,
        UserActivity,
        UserAchievement,
    }
}
