﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class Activity : IActive, IDeletable, ICreated
    {
        public int ActivityId { get; set; }
        public int ActivityGroupId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Points { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public DateTimeOffset Created { get; set; }

        public virtual ActivityGroup ActivityGroup { get; set; }
    
    }
}
