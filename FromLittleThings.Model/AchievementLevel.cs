﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class AchievementLevel : IActive, IDeletable, ICreated
    {
        public int AchievementLevelId { get; set; }
        public int AchievementId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TimelineText { get; set; }
        public int Points { get; set; }
        public string ImageUrl { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }
        public DateTimeOffset Created { get; set; }

        public virtual Achievement Achievement { get; set; }
    
    }
}
