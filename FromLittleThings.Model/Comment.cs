﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Model
{
    public class Comment : IActive, IAction, ICreated, IDeletable, ILikeable
    {
        public int CommentId { get; set; }
        public string UserId { get; set; }
        public Guid EntityRowId { get; set; }
        public int EntityTypeId { get; set; }
        public Guid RowId { get; set; }
        public DateTimeOffset Created { get; set; }
        public string Text { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset? DeletedDate { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

    }
}
