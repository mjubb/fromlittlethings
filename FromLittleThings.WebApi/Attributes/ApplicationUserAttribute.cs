﻿using FromLittleThings.WebApi.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using FromLittleThings.Model;
using FromLittleThings.Repository;

namespace FromLittleThings.WebApi.Attributes
{
    public class ApplicationUserAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        
        public override void  OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ControllerContext.Controller is BaseApiController)
            {
                var controller = actionContext.ControllerContext.Controller as BaseApiController;

                if (controller.User != null)
                {
                    var currentUserId = controller.User.Identity.GetUserId();
                    if (currentUserId != "")
                    {
                        var userRepository = (IRepository<ApplicationUser>) actionContext.ControllerContext.Configuration.DependencyResolver.GetService(typeof(IRepository<ApplicationUser>));

                        var user = userRepository.Find(f => f.Id == currentUserId);
                        if (user != null)
                        {
                            controller.ApplicationUser = user;
                        }
                    }
                }
            }
        }
    }
}