[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(FromLittleThings.WebApi.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(FromLittleThings.WebApi.App_Start.NinjectWebCommon), "Stop")]

namespace FromLittleThings.WebApi.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using FromLittleThings.DataContext;
    using FromLittleThings.Repository;
    using FromLittleThings.Model;
    using FromLittleThings.WebApi.Providers;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<ILittleThingsContext>().To<LittleThingsContext>().InRequestScope();
            kernel.Bind<IApplicationSettings>().To<ApplicationSettings>().InSingletonScope();
            kernel.Bind<IRepository<ActivityGroup>>().To<EFBaseRepository<ActivityGroup>>().InRequestScope();
            kernel.Bind<IRepository<UserActivity>>().To<EFBaseRepository<UserActivity>>().InRequestScope();
            kernel.Bind<IRepository<ApplicationUser>>().To<EFBaseRepository<ApplicationUser>>().InRequestScope();
            kernel.Bind<IRepository<Timeline>>().To<EFBaseRepository<Timeline>>().InRequestScope();
            kernel.Bind<IRepository<Achievement>>().To<EFBaseRepository<Achievement>>().InRequestScope();
            kernel.Bind<IRepository<UserAchievement>>().To<EFBaseRepository<UserAchievement>>().InRequestScope();
            kernel.Bind<IRepository<UserNotification>>().To<EFBaseRepository<UserNotification>>().InRequestScope(); 
            kernel.Bind<IRepository<Friend>>().To<EFBaseRepository<Friend>>().InRequestScope();
            kernel.Bind<IRepository<Like>>().To<EFBaseRepository<Like>>().InRequestScope();
            kernel.Bind<IRepository<Comment>>().To<EFBaseRepository<Comment>>().InRequestScope();

        }        
    }
}
