﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FromLittleThings.WebApi.Providers
{
    public class UserManager : UserManager<ApplicationUser>
    {
        public UserManager(UserStore<ApplicationUser> userStore)
            : base(userStore)
        {

            var dataProtectorProvider = Startup.DataProtectionProvider;
            var dataProtector = dataProtectorProvider.Create("My Asp.Net Identity");
            this.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser, string>(dataProtector)
            {
                TokenLifespan = TimeSpan.FromHours(24),
            };

            EmailService = new EmailService();
        }
    }
}