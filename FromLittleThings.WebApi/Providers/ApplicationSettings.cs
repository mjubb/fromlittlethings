﻿using FromLittleThings.BlobService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FromLittleThings.WebApi
{
    public class ApplicationSettings : IApplicationSettings
    {
        public ApplicationSettings()
        {

            ProfileImageServiceConfiguration = new BlobServiceConfiguration()
            {
                BlobStorageConnectionString = ProfileImageBlobStorageConnectionString,
                BlobStorageContainerName = ProfileImageBlobStorageContainerName,
                BlobStoragePath = ProfileImageBlobStoragePath
            };

            TimelineImageServiceConfiguration = new BlobServiceConfiguration()
            {
                BlobStorageConnectionString = TimelineImageBlobStorageConnectionString,
                BlobStorageContainerName = TimelineImageBlobStorageContainerName,
                BlobStoragePath = TimelineImageBlobStoragePath
            };
        }

        public string AppUrl
        {
            get
            {
                return "http://localhost:8080:/";
            }
        }

        public int ActivityDaysPrevious
        {
            get
            {
                return 3;
            }
        }

        public bool UseAzureBlobStorage
        {
            get
            {
                return GetSetting<bool>("UseAzureBlobStorage", false);
            }
        }

        public string ProfileImageBlobStorageConnectionString
        {
            get
            {
                return GetSetting<string>("ProfileImageBlobStorageConnectionString", string.Empty);
            }
        }

        public string ProfileImageBlobStorageContainerName
        {
            get
            {
                return GetSetting<string>("ProfileImageBlobStorageContainerName", string.Empty);
            }
        }

        public string ProfileImageBlobStoragePath
        {
            get
            {
                return System.Web.Hosting.HostingEnvironment.MapPath("~/ProfileImages");
            }
        }

        public string TimelineImageBlobStorageConnectionString
        {
            get
            {
                return GetSetting<string>("TimelineImageBlobStorageConnectionString", string.Empty);
            }
        }

        public string TimelineImageBlobStorageContainerName
        {
            get
            {
                return GetSetting<string>("TimelineImageBlobStorageContainerName", string.Empty);
            }
        }

        public string TimelineImageBlobStoragePath
        {
            get
            {
                return System.Web.Hosting.HostingEnvironment.MapPath("~/TimelineImages");
            }
        }

        public bool UseCdn
        {
            get
            {
                return GetSetting<bool>("UseCdn", false);
            }
        }

        public string CdnUrl
        {
            get
            {
                return GetSetting<string>("CdnUrl", string.Empty);
            }
        }


        public T GetSetting<T>(string key, T defaultValue)
        {
            if (!string.IsNullOrEmpty(key))
            {
                string value = ConfigurationManager.AppSettings[key];
                try
                {
                    if (value != null)
                    {
                        var theType = typeof(T);
                        if (theType.IsEnum)
                            return (T)Enum.Parse(theType, value.ToString(), true);

                        return (T)Convert.ChangeType(value, theType);
                    }

                    return default(T);
                }
                catch { }
            }

            return defaultValue;
        }

        public IBlobServiceConfiguration ProfileImageServiceConfiguration { get; set; }
        public IBlobServiceConfiguration TimelineImageServiceConfiguration { get; set; }

    }

    public class BlobServiceConfiguration : IBlobServiceConfiguration
    {
        public string BlobStorageConnectionString { get; set; }
        public string BlobStorageContainerName { get; set; }
        public string BlobStoragePath { get; set; }
    }

}