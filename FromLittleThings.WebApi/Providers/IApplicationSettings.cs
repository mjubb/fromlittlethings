﻿using FromLittleThings.BlobService;
using System;
namespace FromLittleThings.WebApi
{
    public interface IApplicationSettings
    {
        int ActivityDaysPrevious { get; }
        string AppUrl { get; }
        string ProfileImageBlobStorageConnectionString { get; }
        string ProfileImageBlobStorageContainerName { get; }
        string ProfileImageBlobStoragePath { get; }
        string TimelineImageBlobStorageConnectionString { get; }
        string TimelineImageBlobStorageContainerName { get; }
        string TimelineImageBlobStoragePath { get; }
        bool UseAzureBlobStorage { get; }
        bool UseCdn { get; }
        string CdnUrl { get; }

        IBlobServiceConfiguration ProfileImageServiceConfiguration { get; }
        IBlobServiceConfiguration TimelineImageServiceConfiguration { get; }

    }


    public interface IBlobServiceConfiguration : IFileSystemBlobServiceConfiguration, IAzureBlobServiceConfiguration
    {
        string BlobStorageConnectionString { get; }
        string BlobStorageContainerName { get; }
        string BlobStoragePath { get; }
    }
}
