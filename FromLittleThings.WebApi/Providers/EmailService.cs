﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;

namespace FromLittleThings.WebApi.Providers
{
    public class EmailService : IIdentityMessageService
    {
        public async Task SendAsync(IdentityMessage message)
        {
//            string sentFrom = ConfigurationManager.AppSettings["EmailSentFrom"] || "noreply@fromlittlethings.com";
            string sentFrom = "noreply@fromlittlethings.com";

            var client = new SmtpClient();

            // Create the message:
            var mail = new System.Net.Mail.MailMessage(sentFrom, message.Destination);

            mail.Subject = message.Subject;
            mail.Body = message.Body;

            // Send:
            await client.SendMailAsync(mail);
        }
    }
}