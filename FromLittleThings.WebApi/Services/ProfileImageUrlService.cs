﻿using FromLittleThings.WebApi.Providers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FromLittleThings.WebApi.Services
{
    public class ProfileImageUrlService
    {
        protected IApplicationSettings Configuration { get; private set; }

        private string ProfileImageUrl = "/api/api/UserProfile/GetProfileImage?fileName=";

        private string DefaultPhoto =  "default-photo.png" ;
        private string DefaultPhotoSmall = "default-photo-thumb.png";

        private bool useCdn = false;

        public ProfileImageUrlService(IApplicationSettings configuration)
        {
            Configuration = configuration;
            useCdn = Configuration.UseCdn;
        }

        public string[] GetUrls(string fileName, bool small = false)
        {
            var urls = new List<string>();

            if (useCdn)
            {
                urls.Add(Path.Combine(Configuration.CdnUrl, fileName));
                useCdn = false; //degrade to local url
            }
            if (!string.IsNullOrWhiteSpace(fileName))
            {
                urls.Add(GetUrl(fileName, small));
            }
            urls.Add(GetUrl(string.Empty, small)); //default photo
            return urls.ToArray();
        }

        public string GetUrl(string fileName, bool small = false)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                if (small)
                {
                    fileName = DefaultPhotoSmall;
                }
                else
                {
                    fileName = DefaultPhoto;
                }
            }

            if (useCdn)
            {
                return Configuration.CdnUrl + fileName;
            }
            else
            {
                return ProfileImageUrl + fileName;
            }
        }


    }
}