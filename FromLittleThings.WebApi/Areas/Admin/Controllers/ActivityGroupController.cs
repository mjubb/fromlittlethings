﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FromLittleThings.Model;
using FromLittleThings.DataContext;

namespace FromLittleThings.WebApi.Areas.Admin.Controllers
{
    public class ActivityGroupController : Controller
    {
        private LittleThingsContext db = new LittleThingsContext();

        // GET: /Admin/ActivityGroup/
        public ActionResult Index()
        {
            return View(db.ActivityGroups.ToList());
        }

        // GET: /Admin/ActivityGroup/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActivityGroup activitygroup = db.ActivityGroups.Find(id);
            if (activitygroup == null)
            {
                return HttpNotFound();
            }
            return View(activitygroup);
        }

        // GET: /Admin/ActivityGroup/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /Admin/ActivityGroup/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ActivityGroupId,Name,Description,IsActive,IsDeleted")] ActivityGroup activitygroup)
        {
            if (ModelState.IsValid)
            {
                db.ActivityGroups.Add(activitygroup);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(activitygroup);
        }

        // GET: /Admin/ActivityGroup/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActivityGroup activitygroup = db.ActivityGroups.Find(id);
            if (activitygroup == null)
            {
                return HttpNotFound();
            }
            return View(activitygroup);
        }

        // POST: /Admin/ActivityGroup/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ActivityGroupId,Name,Description,IsActive,IsDeleted")] ActivityGroup activitygroup)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activitygroup).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(activitygroup);
        }

        // GET: /Admin/ActivityGroup/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ActivityGroup activitygroup = db.ActivityGroups.Find(id);
            if (activitygroup == null)
            {
                return HttpNotFound();
            }
            return View(activitygroup);
        }

        // POST: /Admin/ActivityGroup/Delete/5
        [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ActivityGroup activitygroup = db.ActivityGroups.Find(id);
            db.ActivityGroups.Remove(activitygroup);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
