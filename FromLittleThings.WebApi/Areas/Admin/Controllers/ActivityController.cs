﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FromLittleThings.Model;
using FromLittleThings.DataContext;

namespace FromLittleThings.WebApi.Areas.Admin.Controllers
{
    public class ActivityController : Controller
    {
        private LittleThingsContext db = new LittleThingsContext();

        // GET: /Admin/Activity/
        public ActionResult Index()
        {
            var activities = db.Activities.Include(a => a.ActivityGroup);
            return View(activities.ToList());
        }

        // GET: /Admin/Activity/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // GET: /Admin/Activity/Create
        public ActionResult Create()
        {
            ViewBag.ActivityGroupId = new SelectList(db.ActivityGroups, "ActivityGroupId", "Name");
            return View();
        }

        // POST: /Admin/Activity/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ActivityId,ActivityGroupId,Name,Description,Points,IsActive,IsDeleted,Created")] Activity activity)
        {
            activity.Created = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Activities.Add(activity);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ActivityGroupId = new SelectList(db.ActivityGroups, "ActivityGroupId", "Name", activity.ActivityGroupId);
            return View(activity);
        }

        // GET: /Admin/Activity/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            ViewBag.ActivityGroupId = new SelectList(db.ActivityGroups, "ActivityGroupId", "Name", activity.ActivityGroupId);
            return View(activity);
        }

        // POST: /Admin/Activity/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ActivityId,ActivityGroupId,Name,Description,Points,IsActive,IsDeleted,Created")] Activity activity)
        {
            if (ModelState.IsValid)
            {
                db.Entry(activity).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ActivityGroupId = new SelectList(db.ActivityGroups, "ActivityGroupId", "Name", activity.ActivityGroupId);
            return View(activity);
        }

        // GET: /Admin/Activity/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Activity activity = db.Activities.Find(id);
            if (activity == null)
            {
                return HttpNotFound();
            }
            return View(activity);
        }

        // POST: /Admin/Activity/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Activity activity = db.Activities.Find(id);
            db.Activities.Remove(activity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
