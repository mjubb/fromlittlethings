﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class AchievementController : BaseApiController
    {
        public AchievementController(ILittleThingsContext context)
            : base(context)
        {

        }
        // GET api/<controller>
        [Authorize]
        public IHttpActionResult Get()
        {
            var userAchievements = AchievementService.GetUserAchievements(ApplicationUser);

            return Ok(new
            {
                asAtDate = DateTime.UtcNow,
                achievements = from ua in userAchievements
                               select new
                               {
                                   name = ua.AchievementLevel.Name,
                                   description = ua.AchievementLevel.Description,
                                   imageUrl = ua.AchievementLevel.ImageUrl,
                                   active = true,
                               }
            });
        }


    }
}