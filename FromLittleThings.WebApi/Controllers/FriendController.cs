﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class FriendController : BaseApiController
    {
        public FriendController(ILittleThingsContext context) : base(context)
        {

        }

        // GET api/<controller>
        [Authorize]
        public IHttpActionResult Get()
        {
            var friends = FriendService.GetFriends(ApplicationUser);
            var dto = from f in friends
                          select new
                          {
                              name = f.UserId != ApplicationUser.Id ? f.ApplicationUser.FullName : f.FriendApplicationUser.FullName,
                              friendId = f.FriendId,
                              imageUrls = (f.UserId != ApplicationUser.Id ? ProfileImageUrlService.GetUrls(f.ApplicationUser.ProfileImageThumbUrl, true) : ProfileImageUrlService.GetUrls(f.FriendApplicationUser.ProfileImageThumbUrl, true))
                          };
   


            return Ok(new {
                 asAtDate = DateTime.UtcNow,
                 friends = from f in dto 
                           select new {
                               name = f.name,
                               friendId = f.friendId,
                               imageUrls = f.imageUrls
                           }
            });

        }

        [HttpPut]
        [Authorize]
        public IHttpActionResult Put([FromBody] dynamic model)
        {
            if ((model == null) || (model.email == null))
                return BadRequest("You must enter an email address.");

            var email = model.email.ToString().ToLower();

            try
            {
                FriendService.AddFriend(ApplicationUser, email);
            }
            catch(ApplicationException ex)
            {
                return BadRequest(ex.Message);
            }

            return Ok();

        }

        [HttpDelete]
        [Authorize]
        public IHttpActionResult Delete(int id)
        {
            FriendService.DeleteFriend(ApplicationUser, id);

            return Ok(new 
            {
                success = true
            });
        }

        [HttpPost]
        [Authorize]
        public dynamic SendRecommendEmail([FromBody] dynamic model)
        {
            return new
            {
                success = true
            };
        }

    }
}