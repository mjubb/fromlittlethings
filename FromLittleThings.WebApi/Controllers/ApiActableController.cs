﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    [Authorize]
    public abstract class ApiActableController : BaseApiController
    {
        public ApiActableController(ILittleThingsContext context)
            : base(context)
        {

        }

        public dynamic DeleteLike(int likeId, int itemId)
        {
            var user = ApplicationUser;

            var like = LikeService.GetLike(likeId);

            if ((like != null) && (like.UserId == user.Id))
            {
                LikeService.DeleteLike(like);
            }

            return GetLikes(user.Id, itemId);
        }

        protected object GetLikeObject(IList<Like> likes, ApplicationUser user)
        {
            return new
            {
                count = likes.Count(),
                likeText = likes.Where(l => l.UserId == user.Id).Any() ? "Unlike" : "Like",
                userLikes = likes.Where(l => l.UserId == user.Id).Any(),
                userLikeId = likes.Where(l => l.UserId == user.Id).Any() ? likes.Where(l => l.UserId == user.Id).First().LikeId : 0,
                likes = from l in likes
                        select new
                        {
                            likeId = l.LikeId,
                            userId = l.UserId,
                            fullName = l.ApplicationUser.FullName
                        }
            };
        }

        protected object GetCommentObject(IList<Comment> comments, ApplicationUser user)
        {
            return new
            {
                count = comments.Count(),
                comments = from c in comments
                           select new
                           {
                               commentId = c.CommentId,
                               userId = c.UserId,
                               fullName = c.ApplicationUser.FullName,
                               text = c.Text,
                               created = c.Created,
                               imageUrl = ProfileImageUrlService.GetUrl(c.ApplicationUser.ProfileImageThumbUrl, true),
                               imageUrls = ProfileImageUrlService.GetUrls(c.ApplicationUser.ProfileImageThumbUrl, true),
                               likes = GetLikeObject(LikeService.GetActiveLikes(c), user)
                           }
            };
        }

        [Route("GetLikes")]
        public dynamic GetLikes(string userId, int itemId)
        {
            var item = GetItem<ILikeable>(itemId);

            var likes = LikeService.GetActiveLikes(item);

            var json = GetLikeObject(likes, ApplicationUser);

            return json;

        }

        [Route("GetComments")]
        public dynamic GetComments(string userId, int itemId)
        {
            var item = GetItem<ICommentable>(itemId);

            var comments = CommentService.GetActiveComments(item);

            var json = GetCommentObject(comments, ApplicationUser);

            return json;

        }

        [HttpPost, Route("CreateLike")]
        public virtual IHttpActionResult CreateLike([FromBody] dynamic data)
        {

            var item = (ILikeable)GetItem<ILikeable>((int)data.itemId);

            var existingLike = LikeService.GetActiveLikes(item).Where(u => u.UserId == ApplicationUser.Id).FirstOrDefault();

            var applicationUser = ApplicationUserRepository.Find(u => u.Id == ApplicationUser.Id);

            if (applicationUser == null)
                return BadRequest("Invalid user.");

            if (applicationUser.Id != ApplicationUser.Id)
                return BadRequest("Invalid userId.");

            if (existingLike == null)
            {
                var like = new Like()
                {
                    UserId = applicationUser.Id,
                    ApplicationUser = applicationUser,
                    Created = DateTime.UtcNow,
                    IsActive = true,
                    EntityRowId = item.RowId,
                    EntityTypeId = item.EntityTypeId
                };

                LikeService.CreateLike(like);

            }
            else
            {
                LikeService.DeleteLike(existingLike);
            }

            return Ok(GetLikes(ApplicationUser.Id, (int)data.itemId));

        }

        [HttpPost, Route("CreateComment")]
        public virtual IHttpActionResult CreateComment([FromBody] dynamic data)
        {
            if ((data.text == null) || (string.IsNullOrWhiteSpace((string)data.text)))
                return BadRequest("The comment text is empty.");

            var item = (ICommentable)GetItem<ICommentable>((int)data.itemId);

            var existing = CommentService.GetActiveComments(item).Where(u => u.UserId == ApplicationUser.Id && u.Text == (string)data.text).FirstOrDefault();

            var applicationUser = ApplicationUserRepository.Find(u => u.Id == ApplicationUser.Id);

            if (applicationUser == null)
                return BadRequest("Invalid user.");

            if (applicationUser.Id != ApplicationUser.Id)
                return BadRequest("Invalid userId.");

            if (existing == null)
            {
                var comment = new Comment()
                {
                    UserId = applicationUser.Id,
                    ApplicationUser = applicationUser,
                    Created = DateTime.UtcNow,
                    IsActive = true,
                    Text = (string)data.text,
                    EntityRowId = item.RowId,
                    EntityTypeId = item.EntityTypeId,
                    RowId = Guid.NewGuid()
                };

                CommentService.CreateComment(comment);

                return Ok(GetCommentObject(new List<Comment> { comment }, ApplicationUser));
            }
            else
            {
                return Ok(GetCommentObject(new List<Comment> { existing }, ApplicationUser));
            }
        }

        private T GetItem<T>(int itemId) where T : IActable
        {
            T item = default(T);

            if (this.GetType() == typeof(TimelineController))
            {
                item = (T)(object)TimelineService.GetTimeline(itemId);
            }
            //else if (this.GetType() == typeof(CommentController))
            if (item == null)
            {
                item = (T)(object)CommentService.GetComment(itemId);
            }

            return item;
        }


    }
}
