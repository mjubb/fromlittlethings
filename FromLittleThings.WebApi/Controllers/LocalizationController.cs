﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using FromLittleThings.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class LocalizationController : BaseApiController
    {
        public LocalizationController(ILittleThingsContext context)
            : base(context)
        {

        }

        public dynamic Get(string culture = "en-GB")
        {
            var web = new WebContentResourceService(culture);

            return Ok(new
                   {
                       webContent = web.GetDictionary()
                   });
        }


    }
}