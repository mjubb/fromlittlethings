﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using FromLittleThings.Services;
using FromLittleThings.WebApi.Attributes;
using System.Web.Mvc;
using FromLittleThings.BlobService;
using FromLittleThings.WebApi.Providers;
using FromLittleThings.WebApi.Services;

namespace FromLittleThings.WebApi.Controllers
{
    [ApplicationUserAttribute]
    public class BaseApiController : ApiController
    {

        protected IApplicationSettings ApplicationSettings { get; private set; }
        protected IRepository<ActivityGroup> ActivityGroupRepository;
        protected IRepository<UserActivity> UserActivityRepository;
        protected IRepository<ApplicationUser> ApplicationUserRepository;
        protected IRepository<Timeline> TimelineRepository;
        protected IRepository<Achievement> AchievementRepository;
        protected IRepository<UserAchievement> UserAchievementRepository;
        protected IRepository<Friend> FriendRepository;
        protected IRepository<Like> LikeRepository;
        protected IRepository<UserNotification> UserNotificationRepository;
        protected TimelineService TimelineService { get; private set; }
        protected ApplicationUserService ApplicationUserService { get; private set; }
        protected AchievementService AchievementService { get; private set; }
        protected FriendService FriendService { get; private set; }
        protected LeaderboardService LeaderboardService { get; private set; }
        protected LikeService LikeService { get; private set; }
        protected CommentService CommentService { get; private set; }
        protected IBlobService BlobService { get; private set; }
        protected IBlobService TimelineImageBlobService { get; private set; }
        protected NotificationService NotificationService { get; private set; }
        public ApplicationUser ApplicationUser;
        protected ProfileImageUrlService ProfileImageUrlService { get; private set; }

        public BaseApiController(ILittleThingsContext dbContext)
        {
            ApplicationSettings = DependencyResolver.Current.GetService<IApplicationSettings>();
            TimelineRepository = DependencyResolver.Current.GetService<IRepository<Timeline>>();
            ActivityGroupRepository = DependencyResolver.Current.GetService<IRepository<ActivityGroup>>();
            UserActivityRepository = DependencyResolver.Current.GetService<IRepository<UserActivity>>();
            ApplicationUserRepository = DependencyResolver.Current.GetService<IRepository<ApplicationUser>>();
            AchievementRepository = DependencyResolver.Current.GetService<IRepository<Achievement>>();
            UserAchievementRepository = DependencyResolver.Current.GetService<IRepository<UserAchievement>>();
            FriendRepository = DependencyResolver.Current.GetService<IRepository<Friend>>();
            LikeRepository = DependencyResolver.Current.GetService<IRepository<Like>>();
            UserNotificationRepository = DependencyResolver.Current.GetService<IRepository<UserNotification>>();

            AchievementService = new AchievementService(DependencyResolver.Current.GetService<IRepository<Achievement>>(), DependencyResolver.Current.GetService<IRepository<UserAchievement>>(), DependencyResolver.Current.GetService<IRepository<UserActivity>>(), DependencyResolver.Current.GetService<IRepository<UserNotification>>());
            ApplicationUserService = new ApplicationUserService(DependencyResolver.Current.GetService<IRepository<ApplicationUser>>(), DependencyResolver.Current.GetService<IRepository<UserActivity>>());
            FriendService = new FriendService(DependencyResolver.Current.GetService<IRepository<ApplicationUser>>(), DependencyResolver.Current.GetService<IRepository<Friend>>(), DependencyResolver.Current.GetService<IRepository<UserNotification>>());
            LeaderboardService = new LeaderboardService(DependencyResolver.Current.GetService<IRepository<UserActivity>>());
            LikeService = new LikeService(DependencyResolver.Current.GetService<IRepository<Like>>());
            CommentService = new CommentService(DependencyResolver.Current.GetService<IRepository<Comment>>());
            NotificationService = new NotificationService(DependencyResolver.Current.GetService<IRepository<UserNotification>>());

            if (ApplicationSettings.UseAzureBlobStorage)
            {
                BlobService = new AzureBlobService(ApplicationSettings.ProfileImageServiceConfiguration);
            }
            else
            {
                BlobService = new FileSystemBlobService(ApplicationSettings.ProfileImageServiceConfiguration);
            }
            if (ApplicationSettings.UseAzureBlobStorage)
            {
                TimelineImageBlobService = new AzureBlobService(ApplicationSettings.TimelineImageServiceConfiguration);
            }
            else
            {
                TimelineImageBlobService = new FileSystemBlobService(ApplicationSettings.TimelineImageServiceConfiguration);
            }

            ProfileImageUrlService = new ProfileImageUrlService(ApplicationSettings);
            TimelineService = new TimelineService(DependencyResolver.Current.GetService<IRepository<Timeline>>(), DependencyResolver.Current.GetService<IRepository<ActivityGroup>>(), TimelineImageBlobService);

        } 
    }
}