﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class LeaderboardController : BaseApiController
    {
        public LeaderboardController(ILittleThingsContext context)
            : base(context)
        {

        }

        // GET api/<controller>
        [Authorize]
        public dynamic Get()
        {
            var leaderboard = LeaderboardService.GetLeaderboard(ApplicationUser, null, null);

            return Ok(new
                   {
                       asAtDate = DateTime.UtcNow,
                       leaderboard = from s in leaderboard.Items
                                     select new
                                     {
                                         ranking = s.Ranking,
                                         name = s.ApplicationUser.FullName,
                                         imageUrls = ProfileImageUrlService.GetUrls(s.ApplicationUser.ProfileImageThumbUrl, true),
                                         points = s.Points
                                     }
                   });
        }


    }
}