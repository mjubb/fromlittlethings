﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FromLittleThings.BlobService;
using FromLittleThings.Services.Dto;
using FromLittleThings.Services;

namespace FromLittleThings.WebApi.Controllers
{
    [RoutePrefix("api/timeline")]
    public class TimelineController : ApiActableController
    {


        public TimelineController(ILittleThingsContext context)
            : base(context)
        {

        }

        // GET api/<controller>
        [Authorize]
        public IHttpActionResult Get(int? take = 0, int? before = 0)
        {
            var timeline = TimelineService.GetTimeline(ApplicationUser, take == 0 ? null : take, before == 0 ? null : before);

            return Ok(new
            {
                asAtDate = DateTime.UtcNow,
                user = new
                {
                    id = ApplicationUser.Id,
                    firstName = ApplicationUser.FirstName,
                    lastName = ApplicationUser.LastName,
                    imageUrl = ProfileImageUrlService.GetUrl(ApplicationUser.ProfileImageThumbUrl, true),
                    imageUrls = ProfileImageUrlService.GetUrls(ApplicationUser.ProfileImageThumbUrl, true),
                },
                timeline = from t in timeline
                           select new
                           {
                               timelineId = t.TimelineId,
                               text = t.Text,
                               name = t.ApplicationUser.FullName,
                               created = t.Created,
                               imageUrl = ProfileImageUrlService.GetUrl(t.ApplicationUser.ProfileImageThumbUrl, true),
                               imageUrls = ProfileImageUrlService.GetUrls(t.ApplicationUser.ProfileImageThumbUrl, true),
                               likes = GetLikeObject(LikeService.GetActiveLikes(t), ApplicationUser),
                               comments = GetCommentObject(CommentService.GetActiveComments(t), ApplicationUser)
                           }
            });
        }

        [Authorize]
        public IHttpActionResult Put(dynamic model)
        {
            if (model.text == null && model.file == null)
            {
                return BadRequest("Text or an image must be included.");
            }

            var request = new AddUserPostRequest
            {
                ApplicationUser = ApplicationUser,
                File = (System.IO.FileStream)model.File,
                FileName = "",
                Text = (string)model.text
            };

            var response = TimelineService.AddUserPost(request);

            return Ok(response.Timeline);
        }


    }
}