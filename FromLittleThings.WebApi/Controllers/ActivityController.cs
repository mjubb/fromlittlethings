﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using FromLittleThings.WebApi.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class ActivityController : BaseApiController
    {

        public ActivityController(ILittleThingsContext context)
            : base(context)
        {

        }

        // GET api/<controller>
        [Authorize]
        public IHttpActionResult Get(DateTime? asAtDate = null)
        {
            var asAt = DateTime.UtcNow.Date;
            if (asAtDate.HasValue)
            {
                asAt = asAtDate.Value.Date;
            }
            
            var activityGroups = ActivityGroupRepository.Fetch().Active();
            var userActivities = UserActivityRepository.Fetch().Where(ua => ua.UserId == ApplicationUser.Id).ToList().Where(ua => ua.ActivityDate.Date == asAt);
            var userActivitiesIds = userActivities.Select(a => a.ActivityId).ToList();

            var dto = new
            {
                asAtDate = asAt,
                isEditable = (asAt > DateTime.UtcNow.Date.AddDays(-ApplicationSettings.ActivityDaysPrevious)),
                activities = from ag in activityGroups
                             where ag.IsActive
                             select new
                             {

                                 activityGroupId = ag.ActivityGroupId,
                                 name = ag.Name,
                                 activities = from a in ag.Activities.ToList() 
                                              where a.IsActive && !a.IsDeleted
                                              select new
                                              {
                                                  activityId = a.ActivityId,
                                                  name = a.Name,
                                                  points = a.Points,
                                                  isChecked = userActivitiesIds.Any(ua => ua == a.ActivityId) 
                                              }
                             }
            };
                
            return Ok(dto);
        }

        [HttpPost]
        [Authorize]
        public IHttpActionResult Post([FromBody] dynamic data)
        {
            if (data == null)
                return BadRequest("The submitted data is invalid.");

            var asAtDate = (DateTime)data.asAtDate;

            if (asAtDate.Date < DateTime.UtcNow.Date.AddDays(-ApplicationSettings.ActivityDaysPrevious))
                return BadRequest("You are not able to save your activities for a day so far in the past.");

            var dateStart = asAtDate.Date;
            var dateEnd = asAtDate.Date.AddDays(1);

            var userActivities = UserActivityRepository.Fetch().Where(ua => (ua.ActivityDate >= dateStart) && (ua.ActivityDate < dateEnd) && (ua.UserId == ApplicationUser.Id)).ToList();
            var createdUserActivities = new List<UserActivity>();

            foreach(var activityGroup in data.activities)
            {
                foreach(var activity in activityGroup.activities)
                {
                    if (Convert.ToBoolean(activity.isChecked))
                    {
                        var activityId = (int)activity.activityId;
                        if (!userActivities.Any(ua => ua.ActivityId == activityId))
                        {
                            var dbActivity = ActivityGroupRepository.Fetch().SelectMany(ag => ag.Activities).FirstOrDefault(a => a.ActivityId == activityId );
                            if (dbActivity == null)
                                return BadRequest("You attempted to update invalid data.");

                            var ua = new UserActivity()
                            {
                                ActivityDate = asAtDate,
                                ActivityId = dbActivity.ActivityId,
                                UserId = ApplicationUser.Id,
                                IsActive = true,
                                IsDeleted = false,
                                Created = DateTime.UtcNow,
                                IsApproved = true,
                                Points = dbActivity.Points,
                                RowId = Guid.NewGuid()
                            };

                            createdUserActivities.Add(ua);
                            UserActivityRepository.Add(ua);
                        }
                    }
                    else
                    {
                        var activityId = (int)activity.activityId;
                        userActivities.Where(ua => ua.ActivityId == activityId).ToList().ForEach(u =>
                        {
                            UserActivityRepository.Delete(u);
                        });
                    }
                }
            }

            UserActivityRepository.SaveChanges();

            if (createdUserActivities.Any())
            {
                Task.Factory.StartNew(() => {

                    TimelineService.AddActivitiesToIimeline(ApplicationUser, createdUserActivities);

                    var achievements = AchievementService.CalculatAchievements(ApplicationUser);
                    if (achievements.Any())
                    {
                        TimelineService.AddAchievemensToTimeline(ApplicationUser, achievements);
                    }
                    
                });
            }

            return Ok(new { message = "Saved! Great work!" });

        }
    }
}