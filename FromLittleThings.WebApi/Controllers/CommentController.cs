﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    [RoutePrefix("api/comment")]
    public class CommentController : ApiActableController
    {

        public CommentController(ILittleThingsContext context)
            : base(context)
        {

        }

    }
}