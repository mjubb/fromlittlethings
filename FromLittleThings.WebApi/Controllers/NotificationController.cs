﻿using FromLittleThings.DataContext;
using FromLittleThings.Model;
using FromLittleThings.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FromLittleThings.WebApi.Controllers
{
    public class NotificationController : BaseApiController
    {
        public NotificationController(ILittleThingsContext context)
            : base(context)
        {

        }

        // GET api/<controller>
        [Authorize]
        public dynamic Get()
        {
            var notifications = NotificationService.GetUnreadNotifications(ApplicationUser);

            return Ok(new
                   {
                       asAtDate = DateTime.UtcNow,
                       notifications = from n in notifications
                                     select new
                                     {
                                        text = n.Text
                                     }
                   });
        }


    }
}