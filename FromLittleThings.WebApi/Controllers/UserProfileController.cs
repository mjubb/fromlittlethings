﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using FromLittleThings.Infrastructure;
using System.Net.Http.Headers;
using FromLittleThings.DataContext;
using FromLittleThings.BlobService;
using System.Drawing.Imaging;
using WebApi.OutputCache.V2;


namespace FromLittleThings.WebApi.Controllers
{
    [Authorize]
    public class UserProfileController : BaseApiController
    {
        public UserProfileController(ILittleThingsContext context)
            : base(context)
        {

        }

        public IHttpActionResult Get()
        {
            var totalPoints = ApplicationUserService.GetTotalPoints(ApplicationUser);
            var last3Days = ApplicationUserService.GetPreviousPoints(ApplicationUser, 3);

            return Ok(new
            {
                asAtDate = DateTime.UtcNow,
                firstName = ApplicationUser.FirstName,
                lastName = ApplicationUser.LastName,
                imageUrl =  ProfileImageUrlService.GetUrl(ApplicationUser.ProfileImageUrl),
                imageUrls = ProfileImageUrlService.GetUrls(ApplicationUser.ProfileImageUrl),
                imageThumbUrl = ProfileImageUrlService.GetUrl(ApplicationUser.ProfileImageThumbUrl, true),
                imageThumbUrls = ProfileImageUrlService.GetUrls(ApplicationUser.ProfileImageThumbUrl, true),
                points = totalPoints,
                history = new dynamic[] { 
                     new { name = "Today", points = last3Days.First().Value },
                     new { name = "Yesterday", points = last3Days.Skip(1).Take(1).Single().Value },
                     new { name = last3Days.Skip(2).Take(1).Single().Key.ToString("dddd"), points = last3Days.Skip(2).Take(1).Single().Value }
                }
            });
        }


        [AllowAnonymous]
        [HttpGet]
        [CacheOutput(ClientTimeSpan = 180, ServerTimeSpan = 180)]
        public async Task<IHttpActionResult> GetProfileImage([FromUri] string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
                fileName = "default-photo.png";

            try
            {
                var blob = BlobService.Get(fileName);

                var result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(blob.File.ToArray());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/" + Path.GetExtension(fileName).Replace(".", ""));
                return ResponseMessage(result);
            }
            catch //try default image
            {
                return GetDefaultImage(fileName);
            }


        }


        private IHttpActionResult GetDefaultImage(string fileName)
        {
            var path = System.Web.Hosting.HostingEnvironment.MapPath("~/ProfileImages");
            if (fileName.EndsWith("_sm.jpg"))
            {
                fileName = Path.Combine(path, "default-photo-thumb.png");
            }
            else
            {
                fileName = Path.Combine(path, "default-photo.png");
            }
            if (!File.Exists(fileName))
            {
                return BadRequest("Image not found.");
            }
            else
            {
                var img = Image.FromFile(fileName);
                var ms = new MemoryStream();
                img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                var result = new HttpResponseMessage(HttpStatusCode.OK);
                result.Content = new ByteArrayContent(ms.ToArray());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/" + Path.GetExtension(fileName).Replace(".", ""));
                return ResponseMessage(result);
            }
        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IHttpActionResult> UploadProfileImage()
        {

            if (!Request.Content.IsMimeMultipartContent())
                return StatusCode(HttpStatusCode.UnsupportedMediaType);

            var provider = new MultipartMemoryStreamProvider();
            await Request.Content.ReadAsMultipartAsync(provider);
            foreach (var file in provider.Contents)
            {
                var filename = file.Headers.ContentDisposition.FileName.Trim('\"');
                var buffer = await file.ReadAsByteArrayAsync();

                Image image;
                using (var ms = new MemoryStream(buffer))
                {
                    image = Image.FromStream(ms);

                    var temp = Guid.NewGuid().ToString().Substring(1, 4);
                    var largeFileName = ApplicationUser.Id.ToString() + "_" + temp + ".jpg";
                    var smallFileName = ApplicationUser.Id.ToString() + "_" + temp + "_sm.jpg";

                    var large = image.Resize(160, 160);
                    using (var ls = new MemoryStream())
                    {
                        large.Save(ls, ImageFormat.Jpeg);
                        ls.Seek(0, SeekOrigin.Begin);
                        BlobService.Put(new PutBlobRequest
                        {
                            ContentType = "image/" + Path.GetExtension(largeFileName).Replace(".", ""),
                            FileName = largeFileName,
                            File = ls
                        });
                    }

                    var small = image.Resize(40, 40);
                    using (var ss = new MemoryStream())
                    {
                        small.Save(ss, ImageFormat.Jpeg);
                        ss.Seek(0, SeekOrigin.Begin);
                        BlobService.Put(new PutBlobRequest
                        {
                            ContentType = "image/" + Path.GetExtension(smallFileName).Replace(".", ""),
                            FileName = smallFileName,
                            File = ss
                        });
                    }

                    var newApplicationUser = ApplicationUserRepository.Find(u => u.Id == ApplicationUser.Id);
                    newApplicationUser.ProfileImageUrl = largeFileName;
                    newApplicationUser.ProfileImageThumbUrl = smallFileName;
                    ApplicationUserRepository.SaveChanges();

                    return Ok<dynamic>(new
                    {
                        imageUrls = ProfileImageUrlService.GetUrls(newApplicationUser.ProfileImageUrl)
                    });

                }

            }

            return Ok();
        }
    }
}
