// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BundleConfig.cs" company="">
//   Copyright � 2015 
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace FromLittleThings.Web
{
    using System.Web;
    using System.Web.Optimization;

    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/content/css/app").Include("~/content/app.css").Include("~/content/spinner.css"));

            bundles.Add(new ScriptBundle("~/js/jquery").Include("~/scripts/vendor/jquery-{version}.js"));

            bundles.Add(new Bundle("~/js/app").Include(
                "~/scripts/vendor/angular-ui-router.js",
                "~/scripts/filters.js",
                "~/scripts/moment.js",
                "~/scripts/angular-moment.js",
                "~/scripts/angular-translate.js",
                "~/scripts/FileAPI.min.js",
                "~/scripts/angular-local-storage.js",
                "~/scripts/ng-file-upload-all.js",
                "~/scripts/ng-infinite-scroll.js",
                "~/scripts/angular-ui/ui-bootstrap.js",
                "~/scripts/angular-ui/ui-bootstrap-tpls.js",
                "~/scripts/services.js",
                "~/scripts/directives.js",
                "~/scripts/controllers.js",
                "~/scripts/app.js"
                ));
        }
    }
}
