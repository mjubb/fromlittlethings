'use strict';


// Module specific configuration
angular.module('app.config', [])
  .value('app.config', {
      serviceBase: '/api/'
  });

// Declares how the application should be bootstrapped. See: http://docs.angularjs.org/guide/module
var app = angular.module('app', ['ui.router', 'app.config', 'app.filters', 'app.services', 'app.directives', 'app.controllers', 'angularMoment', 'ui.bootstrap', 'LocalStorageModule', 'ngFileUpload', 'infinite-scroll', 'pascalprecht.translate']);


    // Gets executed during the provider registrations and configuration phase. Only providers and constants can be
    // injected here. This is to prevent accidental instantiation of services before they have been fully configured.
app.config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {

        // UI States, URL Routing & Mapping. For more info see: https://github.com/angular-ui/ui-router
        // ------------------------------------------------------------------------------------------------------------

        $stateProvider
            .state('home', {
                url: '/', 
                templateUrl: '/views/index',
                controller: 'HomeCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { toAbout($q, $state, $timeout, authService) }]
                }
            })
            .state('about', {
                url: '/about',
                templateUrl: '/views/about',
                controller: 'AboutCtrl'
            })
            .state('timeline', {
                url: '/timeline',
                templateUrl: '/views/timeline',
                controller: 'TimelineCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('things', {
                url: '/things',
                templateUrl: '/views/things',
                controller: 'ThingsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('leaderboards', {
                url: '/leaderboards',
                templateUrl: '/views/leaderboards',
                controller: 'LeaderboardsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('rewards', {
                url: '/rewards',
                templateUrl: '/views/rewards',
                controller: 'RewardsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('userProfile', {
                url: '/userProfile',
                templateUrl: '/views/userProfile',
                controller: 'UserProfileCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('frieds', {
                url: '/friends',
                templateUrl: '/views/friends',
                controller: 'FriendCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: '/views/signup',
                controller: 'SignupCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: '/views/login',
                controller: 'LoginCtrl'
            })
            .state('logout', {
                url: '/logout',
                templateUrl: '/views/about',
                controller: 'LogoutCtrl'
            })
            .state('resetPassword', {
                url: '/resetPassword',
                templateUrl: '/views/resetPassword',
                controller: 'ResetPasswordCtrl'
            })
            .state('confirmPassword', {
                url: '/confirmPassword?userId&code',
                templateUrl: '/views/confirmPassword',
                controller: 'ConfirmPasswordCtrl'
            })
            .state('otherwise', {
                url: '*path',
                templateUrl: '/views/404',
                controller: 'Error404Ctrl'
            });


        function toAbout($q, $state, $timeout, authService) {
            if ((authService.authentication) && (authService.authentication.isAuth)) {
                $timeout(function () {
                    $state.go('userProfile')
                });
            } else {
                $timeout(function () {
                    $state.go('about')
                });
            }
        }


        function authenticate($q, $state, $timeout, authService) {
            if ((authService.authentication) && (authService.authentication.isAuth)) {
                return $q.when(authService.authentication.isAuth);
            } else {
                $timeout(function () {
                    $state.go('login')
                });
            }
        }

        $locationProvider.html5Mode(true);

    }])

    // Gets executed after the injector is created and are used to kickstart the application. Only instances and constants
    // can be injected here. This is to prevent further system configuration during application run time.
    .run(['$templateCache', '$rootScope', '$state', '$stateParams', function ($templateCache, $rootScope, $state, $stateParams) {

        // <ui-view> contains a pre-rendered template for the current view
        // caching it will prevent a round-trip to a server at the first page load
        var view = angular.element('#ui-view');
        $templateCache.put(view.data('tmpl-url'), view.html());

        // Allows to retrieve UI Router state information from inside templates
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess', function (event, toState) {

            // Sets the layout name, which can be used to display different layouts (header, footer etc.)
            // based on which page the user is located
            $rootScope.layout = toState.layout;
        });

    }]);


app.run(['$rootScope', 'authService', function ($rootScope, authService) {
    authService.fillAuthData();

    $rootScope.authentication = authService.authentication;

    $rootScope.$watch('authService.authentication.isAuth', function (newVal, oldVal, scope) {
        $rootScope.authentication = authService.authentication;
    })

}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});
;'use strict';
app.factory('achievementService', ['$http', 'app.config', function ($http, config) {

    var achievementServiceFactory = {};

    var _getAchievements= function () {
        return $http.get(config.serviceBase + 'api/achievement').then(function (results) {
            return results;
        });
    };

    achievementServiceFactory.getAchievements = _getAchievements;

    return achievementServiceFactory;

}]);;'use strict';
app.factory('activityService', ['$http', 'app.config', function ($http, config) {

    var activityServiceFactory = {};

    var _getActivities = function (asAtDate) {
        var parameter = asAtDate.toISOString();
        return $http.get(config.serviceBase + 'api/activity?asAtDate=' + parameter).then(function (results) {
            return results;
        });
    };

    var _updateActivities = function (json) {

        return $http.post(config.serviceBase + 'api/activity', json).then(function (results) {
            return results;
        });
    }

    activityServiceFactory.getActivities = _getActivities;
    activityServiceFactory.updateActivities = _updateActivities;

    return activityServiceFactory;

}]);;'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {

    var authInterceptorServiceFactory = {};

    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers.Authorization = 'Bearer ' + authData.token;
            config.headers.UserId = authData.userId;
        }

        return config;
    }

    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            $location.path('/login');
        }
        return $q.reject(rejection);
    }

    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);;'use strict';


angular.module('app').factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/api/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        // _logOut();

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/register', registration).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            deferred.reject(err);
        });

        return deferred.promise;
    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userId: response.userId, userName: loginData.userName });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/logout', null, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            deferred.reject(err);
        });

        _authentication.isAuth = false;
        _authentication.userName = "";

        return deferred.promise;
    };

    var _resetPassword = function (json) {

        var deferred = $q.defer();

        try {
            $http.post(serviceBase + 'api/account/forgotPassword', json, {}).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
        }
        catch (ex1) {
            var response = {
                err_description: 'An error occured processing the request.'
            };
            deferred.reject(response);
        }

        return deferred.promise;
    };

    var _confirmPassword = function (json) {

        var deferred = $q.defer();

        try {
            $http.post(serviceBase + 'api/account/confirmPassword', json, {}).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
        }
        catch (ex1) {
            var response = {
                err_description: 'An error occured processing the request.'
            };
            deferred.reject(response);
        }

        return deferred.promise;
    }

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
    }

    var _getAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (!authData)
        {
            authData = {
                isAuth: false
            };
        }
        return authData;
    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.resetPassword = _resetPassword;
    authServiceFactory.confirmPassword = _confirmPassword;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.getAuthData = _getAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);;'use strict';
app.factory('commentService', ['$http', 'app.config', function ($http, config) {

    var commentServiceFactory = {};

    var _createComment = function (itemId, text) {

        var data = {
            itemId: itemId,
            text: text
        };

        return $http.post(config.serviceBase + 'api/timeline/CreateComment', data).then(function (results) {
            return results;
        });
    };

    commentServiceFactory.createComment = _createComment;

    return commentServiceFactory;

}]);;'use strict';

app.directive('commentsDirective', ['commentService', function (commentService) {
    return {
        restrict: 'E',
        scope: {
            comments: '=',
            id: '=',
            user: '=',
        },
        templateUrl: '/Scripts/templates/comments.html',
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            scope.newComment = '';
            scope.showSpinner = false;
            scope.addComment = function () {
                if (scope.newComment) {
                    scope.showSpinner = true;
                    commentService.createComment(scope.id, scope.newComment).then(function (result) {
                        scope.comments.comments.push(result.data.comments[0]);
                        scope.newComment = '';
                        scope.showSpinner = false;
                    }, function(error) {
                        scope.showSpinner = false;
                    });
                }
            };
        }
    }
}]);;'use strict';

angular.module('app.controllers')

    .controller('ConfirmPasswordCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.hideLoginMenu = true;
        $scope.confirmPasswordModel = {};
        $scope.confirmPasswordModel.userId = $location.search().userId || '';
        $scope.confirmPasswordModel.token = $location.search().code || '';
        $scope.confirmPasswordModel.newPassword = '';
        $scope.showSpinner = false;

        $scope.isValid = function () {
            return ($scope.confirmPasswordModel.userId != false) && ($scope.confirmPasswordModel.token != false);
        };

        $scope.confirmPassword = function () {
            $scope.showError = false;
            $scope.showSuccess = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.confirmPasswordModel);
            authService.confirmPassword(json).then(function (response) {
                $scope.showSpinner = false;
                $scope.showSuccess = true;
                $timeout(function () {
                    $location.path("/login");
                }, 1000);
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.showSuccess = false;
                 $scope.errorMessage = err.error_description || err.Message || 'An unknown error occured.';
             });
        };
    }]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('FriendCtrl', ['$scope', '$location', '$window', '$rootScope', 'friendService', function ($scope, $location, $window, $rootScope, friendService) {
        $scope.$root.title = 'From Little Things: Timeline';

        $scope.friendModel = {};
        $scope.addModel = {};

        $scope.showSpinner = true;

        $scope.getFriends = function () {
            friendService.getFriends().then(function (result) {
                $scope.friendModel.friends = result.data.friends;
                $scope.showSpinner = false;
            }, function(e){
                $scope.showSpinner = false;
                $scope.friendModel.error = true;
            });
        };

        $scope.getFriends();

        $scope.addFriend = function () {
            $scope.addModel.error = false;
            $scope.addModel.success = false;
            $scope.showAddSpinner = true;
            friendService.addFriend($scope.addModel.email).then(function (result) {
                $scope.addModel.success = true;
                $scope.getFriends();
                $scope.showAddSpinner = false;
            }, function (error) {
                $scope.addModel.error = true;
                $scope.addModel.errorMessage = error.data.Message || 'An error occured attempting to process your request.';
                $scope.showAddSpinner = false;
            });
        }

        $scope.deleteFriend = function (id) {
            if (confirm('Are you sure you want to remove this person from your friends?')) {
                $scope.showSpinner = true;
                friendService.deleteFriend(id).then(function (result) {
                    $scope.getFriends();
                });
            }
        }

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);;'use strict';
app.factory('friendService', ['$http', 'app.config', function ($http, config) {

    var friendServiceFactory = {};

    var _getFriends = function () {
        return $http.get(config.serviceBase + 'api/friend').then(function (results) {
            return results;
        });
    };

    var _addFriend = function (email) {

        var data = {
            email: email
        };

        return $http.put(config.serviceBase + 'api/friend', data).then(function (results) {
            return results;
        });
    };

    var _deleteFriend = function (id) {

        return $http.delete(config.serviceBase + 'api/friend/' + id).then(function (results) {
            return results;
        });
    };

    friendServiceFactory.getFriends = _getFriends;
    friendServiceFactory.addFriend = _addFriend;
    friendServiceFactory.deleteFriend = _deleteFriend;

    return friendServiceFactory;

}]);;'use strict';

app.directive('gracefulImage', [function () {
    return {
        restrict: 'E',
        scope: {
            srcs: '='
        },
        template: '<img  />',
        replace: true,
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            
            scope.index = 0;
            scope.urls = [];
            scope.$watch('srcs', function () {
                if (scope.srcs && scope.srcs.length) {
                    attrs.$set('src', scope.srcs[scope.index]);                    
                }
            });

            elem.bind('error', function () {
                if (scope.index <= scope.srcs.length) {
                    scope.index++;
                    attrs.$set('src', scope.srcs[scope.index]);
                }
            });
        }
    }
}]);;'use strict';
app.factory('leaderboardService', ['$http', 'app.config', function ($http, config) {

    var leaderboardServiceFactory = {};

    var _getLeaderboards = function () {
        return $http.get(config.serviceBase + 'api/leaderboard').then(function (results) {
            return results;
        });
    };

    leaderboardServiceFactory.getLeaderboards = _getLeaderboards;

    return leaderboardServiceFactory;

}]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('LeaderboardsCtrl', ['$scope', '$location', '$window', '$rootScope', 'leaderboardService', function ($scope, $location, $window, $rootScope, leaderboardService) {
        $scope.$root.title = 'From Little Things: Leaderboards';

        $scope.leaderboardModel = {};

        $scope.getLeaderboards = function () {
            leaderboardService.getLeaderboards().then(function (result) {
                $scope.leaderboardModel.leaderboard = result.data.leaderboard;
                $scope.showSpinner = false;
            });
        };

        $scope.showSpinner = true;
        $scope.getLeaderboards();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);;'use strict';

app.directive('like', ['likeService', function (likeService) {
    return {
        restrict: 'E',
        scope: {
            likes: '=',
            id: '='
        },
        template: '<a ng-click="doLike()" class="like" ng-hide="showSpinner"></a><spinner show="showSpinner" style="display: inline; width: 20px; height: 20px; margin: 0; padding: 0;" /><span ng-bind="getLikesText()"></span>',
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            var span = elem.find('a');
            scope.showSpinner = false;
            scope.doLike = function () {
                scope.showSpinner = true;
                likeService.createLike(scope.id).then(function (result) {
                    scope.likes = result.data;
                    scope.showSpinner = false;
                }, function(error) {
                    scope.showSpinner = false;   
                });
            };

            scope.getLikesText = function() {
                if (scope.likes.likes.length == 0)
                    return "Be the first to like this.";
                else if (!scope.likes.userLikes)
                    return scope.likes.likes.length + " people like this";
                else if (scope.likes.userLikes && scope.likes.likes.length == 1)
                    return "You like this.";
                else
                    return "You and " + parseInt(scope.likes.likes.length - 1) + " people like this.";

            }

        }
    }
}]);;'use strict';
app.factory('likeService', ['$http', 'app.config', function ($http, config) {

    var likeServiceFactory = {};

    var _createLike = function (itemId) {

        var data = {
            itemId: itemId
        };

        return $http.post(config.serviceBase + 'api/timeline/CreateLike', data).then(function (results) {
            return results;
        });
    };

    likeServiceFactory.createLike = _createLike;

    return likeServiceFactory;

}]);;'use strict';

angular.module('app.controllers')

    .controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.loginData = {
            userName: "",
            password: ""
        };
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.message = "";
        $scope.hideLoginMenu = true;
        $scope.showSpinner = false;

        $scope.login = function () {
            $scope.showError = false;
            $scope.showSpinner = true;
            authService.login($scope.loginData).then(function (response) {
                $scope.showSpinner = false;
                $location.path('/');
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.errorMessage = err.error_description;
             });
        };
    }]);;'use strict';

angular.module('app.controllers')

    .controller('LogoutCtrl', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

        authService.logOut().then(function (response) {
            $location.path('/');
        });

    }]);;'use strict';
app.factory('notificationService', ['$http', 'app.config', function ($http, config) {

    var notificationServiceFactory = {};

    var _getUnreadNotifications = function () {
        return $http.get(config.serviceBase + 'api/notification').then(function (results) {
            return results;
        });
    };

    notificationServiceFactory.getUnreadNotifications = _getUnreadNotifications;

    return notificationServiceFactory;

}]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('NotificationsCtrl', ['$scope', '$location', '$window', '$rootScope', '$interval', 'notificationService', 'authService', function ($scope, $location, $window, $rootScope, $interval, notificationService, authService) {

        $scope.notifications = [];

        $scope.getNotifications = function () {

            var auth = authService.getAuthData();
        
            if (auth && auth.token) {
                notificationService.getUnreadNotifications().then(function (result) {
                    $scope.notifications = result.data.notifications;
                });
            }
            else {
                $scope.notifications = [];
            }
        };

        $scope.closeAlert = function (index) {
            if ($scope.notifications.length > index) {
                $scope.notifications.splice(index, 1);
            }
        }

        $rootScope.$on('checkNotifications', function () {
            $scope.getNotifications();
        });

        $interval(function () {
            $scope.getNotifications();
        }, 60000);

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);;'use strict';

angular.module('app.controllers')

    .controller('ResetPasswordCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.hideLoginMenu = true;
        $scope.resetPasswordModel = {};
        $scope.resetPasswordModel.email = '';
        $scope.showSpinner = false;

        $scope.resetPassword = function () {
            $scope.showError = false;
            $scope.showSuccess = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.resetPasswordModel);
            authService.resetPassword(json).then(function (response) {
                $scope.showSpinner = false;
                $scope.showSuccess = true;
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.showSuccess = false;
                 $scope.errorMessage = err.error_description || err.ExceptionMessage || 'An unknown error occured.';
             });
        };
    }]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('RewardsCtrl', ['$scope', '$location', '$window', '$rootScope', 'achievementService', function ($scope, $location, $window, $rootScope, achievementService) {
        $scope.$root.title = 'From Little Things: Animals';

        $scope.achievementModel = {};

        $scope.getAchievements = function () {
            achievementService.getAchievements().then(function (result) {
                $scope.achievementModel.achievements = result.data.achievements;
                $scope.showSpinner = false;
            });
        };

        $scope.showSpinner = true;
        $scope.getAchievements();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);;'use strict';

angular.module('app.controllers')

    .controller('SignupCtrl', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.message = "";
        $scope.hideSignUpMenu = true;
        $scope.registration = {
            userName: "",
            password: "",
            firstName: "",
            lastName: "",
            confirmPassword: ""
        };

        $scope.signUp = function () {

            $scope.showError = false;
            if ($scope.signUpForm.$valid) {
                authService.saveRegistration($scope.registration).then(function (response) {
                    $scope.showSuccess = true;
                    authService.login($scope.registration).then(function (response2) {
                        $scope.message = "Your account has been created, you are now being signed in.";
                        startTimer();
                    });
                },
                 function (response) {
                     var errors = [];
                     if (response.ModelState) {
                         for (var key in response.ModelState) {
                             for (var i = 0; i < response.ModelState[key].length; i++) {
                                 errors.push(response.ModelState[key][i]);
                             }
                             $scope.message = "Your account was not created. Please correct the following errors:" + errors.join(' ');
                         }
                     }
                     else {
                         $scope.message = response.Message || 'An unknown error occurred. Please try again later.';
                     }
                     $scope.showError = true;
                 });
            }
            else {
                $scope.showError = true;
                $scope.message = "Please correct the errors and try again.";
            }
        };

        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $location.path('/');
            }, 1000);
        }

    }]);;'use strict';

app.directive('spinner', [function () {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        template: '<img src="./Images/green-spinner.gif" class="mini-spinner text-center" ng-show="show" />',
        replace: true,
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            

        }
    }
}]);;
angular.module('app.controllers')
    // Path: /news
    .controller('ThingsCtrl', ['$scope', '$location', '$window', '$rootScope', '$timeout', 'activityService', function ($scope, $location, $window, $rootScope, $timeout, activityService) {
        $scope.$root.title = 'From Little Things: Things';

        $scope.response = {}
        $scope.response.showMessage = false;
        $scope.response.showError = false;
        $scope.response.fadeAlert = false;

        $scope.activityModel = {}
        $scope.activityModel.asAtDate = new Date();
        $scope.activityModel.activities = {};

        $scope.canGoPreviousDate = true;


        $scope.checkSave = function () {
            return $scope.activityModel.asAtDate >= moment(new Date()).subtract(3, 'days').toDate();
        };

        $scope.checkNextDate = function () {
            return moment($scope.activityModel.asAtDate).isBefore(moment(new Date()), 'day');
        };

        $scope.canGoNextDate = $scope.checkNextDate();
        $scope.canSave = $scope.checkSave();

        $scope.previousDay = function () {
            $scope.showSpinner = true;
            $scope.activityModel.asAtDate = moment($scope.activityModel.asAtDate).subtract(1, 'days').toDate();
            $scope.canSave = $scope.checkSave();
            $scope.canGoNextDate = $scope.checkNextDate();
            $scope.getActivities();
        };
        $scope.nextDay = function () {
            $scope.showSpinner = true;
            $scope.activityModel.asAtDate = moment($scope.activityModel.asAtDate).add(1, 'days').toDate();
            $scope.canSave = $scope.checkSave();
            $scope.canGoNextDate = $scope.checkNextDate();
            $scope.getActivities();
        };


        $scope.getActivities = function () {
            activityService.getActivities($scope.activityModel.asAtDate).then(function (result) {
                $scope.activityModel.activities = result.data.activities;
                $scope.activityModel.asAtDate = result.data.asAtDate;
                $scope.isEditable = result.data.isEditable;
                $scope.showSpinner = false;
            }, function(error) {
                $scope.showSpinner = false;
            });
        };

        $scope.updateActivities = function () {
            $scope.response.fadeAlert = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.activityModel);
            activityService.updateActivities(json).then(function (result) {
                $scope.response.success = result.data.success;
                $scope.setSuccess(result.data.message)
                $timeout(function () {
                    $scope.response.fadeAlert = true;
                }, 3000).then(function () {
                    $timeout(function () {
                        $scope.response.showMessage = false;
                    }, 500);
                });
                $rootScope.$broadcast('checkNotifications');
            }, function(error) {
                $scope.setError(error.data.Message || 'An error occured processing the request.');
            });
        };

        $scope.setSuccess = function (message) {
            $scope.response.message = message;
            $scope.showSpinner = false;
            $scope.response.showError = false;
            $scope.response.showMessage = true;
        };

        $scope.setError = function (message) {
            $scope.showSpinner = false;
            $scope.response.showError = true;
            $scope.response.showMessage = false;
            $scope.response.errorMessage = message;
        };

        $scope.showSpinner = true;
        $scope.getActivities();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('TimelineCtrl', ['$scope', '$location', '$window', '$rootScope', 'timelineService', function ($scope, $location, $window, $rootScope, timelineService) {
        $scope.$root.title = 'From Little Things: Timeline';

        $scope.timelineModel = {};
        $scope.timelineModel.take = 10;
        $scope.timelineModel.before = 0;
        $scope.timelineModel.noResults = false;
        $scope.isScrolling = false;
        $scope.user = {};

        $scope.getTimeline = function () {
            timelineService.getTimeline($scope.timelineModel.take, $scope.timelineModel.before).then(function (result) {

                if (!$scope.timelineModel.timeline || !$scope.timelineModel.timeline.length) {
                    if (result.data.timeline && result.data.timeline.length) {
                        $scope.timelineModel.timeline = result.data.timeline;
                        $scope.user = result.data.user;
                    }
                    else {
                        $scope.timelineModel.noResults = true;
                    }
                }
                else {
                    for (var i = 0; i < result.data.timeline.length; i++) {
                        $scope.timelineModel.timeline.push(result.data.timeline[i]);
                    }
                }

                if ($scope.timelineModel.timeline && $scope.timelineModel.timeline.length) {
                    $scope.timelineModel.before = $scope.timelineModel.timeline[$scope.timelineModel.timeline.length - 1].timelineId;
                }
                $scope.isScrolling = false;

            }, function (error) {
                $scope.showError = true;
                $scope.message = error.data.Message || 'An error occured loading the timeline.';
                $scope.isScrolling = false;
            });
        }

        $scope.scroll = function () {
            $scope.isScrolling = true;
            $scope.getTimeline();
        };

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);;'use strict';
app.factory('timelineService', ['$http', 'app.config', function ($http, config) {


    var timelineServiceFactory = {};

    var _getTimeline = function (take, before) {
        return $http.get(config.serviceBase + 'api/timeline?take='+take+'&before='+before).then(function (results) {
            return results;
        });
    };

    timelineServiceFactory.getTimeline = _getTimeline;

    return timelineServiceFactory;

}]);;'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('UserProfileCtrl', ['$scope', '$location', '$window', '$rootScope', 'authService', 'userProfileService', 'Upload', function ($scope, $location, $window, $rootScope, authService, userProfileService, Upload) {
        $scope.$root.title = 'From Little Things: Profile';

        $scope.userProfileModel = {};
        $scope.userProfileModel.imageUrl = '';
        $scope.getUserProfile = function () {
            userProfileService.getUserProfile().then(function (result) {
                $scope.userProfileModel.firstName = result.data.firstName;
                $scope.userProfileModel.lastName = result.data.lastName;
                $scope.userProfileModel.imageUrl = result.data.imageUrl;
                $scope.userProfileModel.imageUrls = result.data.imageUrls;
                $scope.userProfileModel.imageThumbUrl = result.data.imageThumbUrl;
                $scope.userProfileModel.imageThumbUrls = result.data.imageThumbUrl;
                $scope.userProfileModel.points = result.data.points;
                $scope.userProfileModel.history = result.data.history;
                $scope.showSpinner = false;
            }, function(error) {
                $scope.showSpinner = false;
            });
        };

        $scope.logOut = function () {
            authService.logOut();
            $location.path('/about');
        }

        $scope.authentication = authService.authentication;

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.upload = function (files) {
            if (files && files.length) {
                $scope.showUploadSpinner = true;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    Upload.upload({
                        url: '/api/api/userProfile/UploadProfileImage',
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                       // console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        //$scope.userProfileModel.imageUrl = '//:0';
                        $scope.userProfileModel.imageUrls = data.imageUrls;
                        $scope.showUploadSpinner = false;
                        //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    }).error(function (er) {
                        $scope.showUploadSpinner = false;
                    });
                }
            }
        };

        $scope.showSpinner = true;
        $scope.getUserProfile();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);;'use strict';
app.factory('userProfileService', ['$http', 'app.config', function ($http, config) {

    var userProfileServiceFactory = {};

    var _getUserProfile = function () {
        return $http.get(config.serviceBase + 'api/userProfile').then(function (results) {
            return results;
        });
    };

    var _uploadUserProfileImage = function (data) {
        return $http.post(config.serviceBase + 'api/userProfile/UploadProfileImage', data).then(function (results) {
            return results;
        });
    };

    userProfileServiceFactory.getUserProfile = _getUserProfile;
    userProfileServiceFactory.uploadUserProfileImage = _uploadUserProfileImage;

    return userProfileServiceFactory;

}]);