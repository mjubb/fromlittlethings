﻿'use strict';
app.factory('likeService', ['$http', 'app.config', function ($http, config) {

    var likeServiceFactory = {};

    var _createLike = function (itemId) {

        var data = {
            itemId: itemId
        };

        return $http.post(config.serviceBase + 'api/timeline/CreateLike', data).then(function (results) {
            return results;
        });
    };

    likeServiceFactory.createLike = _createLike;

    return likeServiceFactory;

}]);