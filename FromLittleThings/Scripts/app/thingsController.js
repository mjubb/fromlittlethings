﻿
angular.module('app.controllers')
    // Path: /news
    .controller('ThingsCtrl', ['$scope', '$location', '$window', '$rootScope', '$timeout', 'activityService', function ($scope, $location, $window, $rootScope, $timeout, activityService) {
        $scope.$root.title = 'From Little Things: Things';

        $scope.response = {}
        $scope.response.showMessage = false;
        $scope.response.showError = false;
        $scope.response.fadeAlert = false;

        $scope.activityModel = {}
        $scope.activityModel.asAtDate = new Date();
        $scope.activityModel.activities = {};

        $scope.canGoPreviousDate = true;


        $scope.checkSave = function () {
            return $scope.activityModel.asAtDate >= moment(new Date()).subtract(3, 'days').toDate();
        };

        $scope.checkNextDate = function () {
            return moment($scope.activityModel.asAtDate).isBefore(moment(new Date()), 'day');
        };

        $scope.canGoNextDate = $scope.checkNextDate();
        $scope.canSave = $scope.checkSave();

        $scope.previousDay = function () {
            $scope.showSpinner = true;
            $scope.activityModel.asAtDate = moment($scope.activityModel.asAtDate).subtract(1, 'days').toDate();
            $scope.canSave = $scope.checkSave();
            $scope.canGoNextDate = $scope.checkNextDate();
            $scope.getActivities();
        };
        $scope.nextDay = function () {
            $scope.showSpinner = true;
            $scope.activityModel.asAtDate = moment($scope.activityModel.asAtDate).add(1, 'days').toDate();
            $scope.canSave = $scope.checkSave();
            $scope.canGoNextDate = $scope.checkNextDate();
            $scope.getActivities();
        };


        $scope.getActivities = function () {
            activityService.getActivities($scope.activityModel.asAtDate).then(function (result) {
                $scope.activityModel.activities = result.data.activities;
                $scope.activityModel.asAtDate = result.data.asAtDate;
                $scope.isEditable = result.data.isEditable;
                $scope.showSpinner = false;
            }, function(error) {
                $scope.showSpinner = false;
            });
        };

        $scope.updateActivities = function () {
            $scope.response.fadeAlert = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.activityModel);
            activityService.updateActivities(json).then(function (result) {
                $scope.response.success = result.data.success;
                $scope.setSuccess(result.data.message)
                $timeout(function () {
                    $scope.response.fadeAlert = true;
                }, 3000).then(function () {
                    $timeout(function () {
                        $scope.response.showMessage = false;
                    }, 500);
                });
                $rootScope.$broadcast('checkNotifications');
            }, function(error) {
                $scope.setError(error.data.Message || 'An error occured processing the request.');
            });
        };

        $scope.setSuccess = function (message) {
            $scope.response.message = message;
            $scope.showSpinner = false;
            $scope.response.showError = false;
            $scope.response.showMessage = true;
        };

        $scope.setError = function (message) {
            $scope.showSpinner = false;
            $scope.response.showError = true;
            $scope.response.showMessage = false;
            $scope.response.errorMessage = message;
        };

        $scope.showSpinner = true;
        $scope.getActivities();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);