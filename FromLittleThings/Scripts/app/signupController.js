﻿'use strict';

angular.module('app.controllers')

    .controller('SignupCtrl', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.message = "";
        $scope.hideSignUpMenu = true;
        $scope.registration = {
            userName: "",
            password: "",
            firstName: "",
            lastName: "",
            confirmPassword: ""
        };

        $scope.signUp = function () {

            $scope.showError = false;
            if ($scope.signUpForm.$valid) {
                authService.saveRegistration($scope.registration).then(function (response) {
                    $scope.showSuccess = true;
                    authService.login($scope.registration).then(function (response2) {
                        $scope.message = "Your account has been created, you are now being signed in.";
                        startTimer();
                    });
                },
                 function (response) {
                     var errors = [];
                     if (response.ModelState) {
                         for (var key in response.ModelState) {
                             for (var i = 0; i < response.ModelState[key].length; i++) {
                                 errors.push(response.ModelState[key][i]);
                             }
                             $scope.message = "Your account was not created. Please correct the following errors:" + errors.join(' ');
                         }
                     }
                     else {
                         $scope.message = response.Message || 'An unknown error occurred. Please try again later.';
                     }
                     $scope.showError = true;
                 });
            }
            else {
                $scope.showError = true;
                $scope.message = "Please correct the errors and try again.";
            }
        };

        var startTimer = function () {
            var timer = $timeout(function () {
                $timeout.cancel(timer);
                $location.path('/');
            }, 1000);
        }

    }]);