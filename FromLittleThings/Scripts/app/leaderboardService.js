﻿'use strict';
app.factory('leaderboardService', ['$http', 'app.config', function ($http, config) {

    var leaderboardServiceFactory = {};

    var _getLeaderboards = function () {
        return $http.get(config.serviceBase + 'api/leaderboard').then(function (results) {
            return results;
        });
    };

    leaderboardServiceFactory.getLeaderboards = _getLeaderboards;

    return leaderboardServiceFactory;

}]);