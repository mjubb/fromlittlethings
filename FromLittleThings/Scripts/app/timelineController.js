﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('TimelineCtrl', ['$scope', '$location', '$window', '$rootScope', 'timelineService', function ($scope, $location, $window, $rootScope, timelineService) {
        $scope.$root.title = 'From Little Things: Timeline';

        $scope.timelineModel = {};
        $scope.timelineModel.take = 10;
        $scope.timelineModel.before = 0;
        $scope.timelineModel.noResults = false;
        $scope.isScrolling = false;
        $scope.user = {};

        $scope.getTimeline = function () {
            timelineService.getTimeline($scope.timelineModel.take, $scope.timelineModel.before).then(function (result) {

                if (!$scope.timelineModel.timeline || !$scope.timelineModel.timeline.length) {
                    if (result.data.timeline && result.data.timeline.length) {
                        $scope.timelineModel.timeline = result.data.timeline;
                        $scope.user = result.data.user;
                    }
                    else {
                        $scope.timelineModel.noResults = true;
                    }
                }
                else {
                    for (var i = 0; i < result.data.timeline.length; i++) {
                        $scope.timelineModel.timeline.push(result.data.timeline[i]);
                    }
                }

                if ($scope.timelineModel.timeline && $scope.timelineModel.timeline.length) {
                    $scope.timelineModel.before = $scope.timelineModel.timeline[$scope.timelineModel.timeline.length - 1].timelineId;
                }
                $scope.isScrolling = false;

            }, function (error) {
                $scope.showError = true;
                $scope.message = error.data.Message || 'An error occured loading the timeline.';
                $scope.isScrolling = false;
            });
        }

        $scope.scroll = function () {
            $scope.isScrolling = true;
            $scope.getTimeline();
        };

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);