﻿'use strict';


angular.module('app').factory('authService', ['$http', '$q', 'localStorageService', function ($http, $q, localStorageService) {

    var serviceBase = '/api/';
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName: ""
    };

    var _saveRegistration = function (registration) {

        // _logOut();

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/register', registration).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            deferred.reject(err);
        });

        return deferred.promise;
    };

    var _login = function (loginData) {

        var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();

        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

            localStorageService.set('authorizationData', { token: response.access_token, userId: response.userId, userName: loginData.userName });

            _authentication.isAuth = true;
            _authentication.userName = loginData.userName;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');

        var deferred = $q.defer();

        $http.post(serviceBase + 'api/account/logout', null, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {
            deferred.resolve(response);
        }).error(function (err, status) {
            deferred.reject(err);
        });

        _authentication.isAuth = false;
        _authentication.userName = "";

        return deferred.promise;
    };

    var _resetPassword = function (json) {

        var deferred = $q.defer();

        try {
            $http.post(serviceBase + 'api/account/forgotPassword', json, {}).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
        }
        catch (ex1) {
            var response = {
                err_description: 'An error occured processing the request.'
            };
            deferred.reject(response);
        }

        return deferred.promise;
    };

    var _confirmPassword = function (json) {

        var deferred = $q.defer();

        try {
            $http.post(serviceBase + 'api/account/confirmPassword', json, {}).success(function (response) {
                deferred.resolve(response);
            }).error(function (err, status) {
                deferred.reject(err);
            });
        }
        catch (ex1) {
            var response = {
                err_description: 'An error occured processing the request.'
            };
            deferred.reject(response);
        }

        return deferred.promise;
    }

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData) {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
        }
    }

    var _getAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (!authData)
        {
            authData = {
                isAuth: false
            };
        }
        return authData;
    }

    authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.resetPassword = _resetPassword;
    authServiceFactory.confirmPassword = _confirmPassword;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.getAuthData = _getAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);