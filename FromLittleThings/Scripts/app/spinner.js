﻿'use strict';

app.directive('spinner', [function () {
    return {
        restrict: 'E',
        scope: {
            show: '='
        },
        template: '<img src="./Images/green-spinner.gif" class="mini-spinner text-center" ng-show="show" />',
        replace: true,
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            

        }
    }
}]);