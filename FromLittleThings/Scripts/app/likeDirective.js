﻿'use strict';

app.directive('like', ['likeService', function (likeService) {
    return {
        restrict: 'E',
        scope: {
            likes: '=',
            id: '='
        },
        template: '<a ng-click="doLike()" class="like" ng-hide="showSpinner"></a><spinner show="showSpinner" style="display: inline; width: 20px; height: 20px; margin: 0; padding: 0;" /><span ng-bind="getLikesText()"></span>',
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            var span = elem.find('a');
            scope.showSpinner = false;
            scope.doLike = function () {
                scope.showSpinner = true;
                likeService.createLike(scope.id).then(function (result) {
                    scope.likes = result.data;
                    scope.showSpinner = false;
                }, function(error) {
                    scope.showSpinner = false;   
                });
            };

            scope.getLikesText = function() {
                if (scope.likes.likes.length == 0)
                    return "Be the first to like this.";
                else if (!scope.likes.userLikes)
                    return scope.likes.likes.length + " people like this";
                else if (scope.likes.userLikes && scope.likes.likes.length == 1)
                    return "You like this.";
                else
                    return "You and " + parseInt(scope.likes.likes.length - 1) + " people like this.";

            }

        }
    }
}]);