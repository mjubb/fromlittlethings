﻿'use strict';
app.factory('notificationService', ['$http', 'app.config', function ($http, config) {

    var notificationServiceFactory = {};

    var _getUnreadNotifications = function () {
        return $http.get(config.serviceBase + 'api/notification').then(function (results) {
            return results;
        });
    };

    notificationServiceFactory.getUnreadNotifications = _getUnreadNotifications;

    return notificationServiceFactory;

}]);