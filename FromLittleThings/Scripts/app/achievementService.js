﻿'use strict';
app.factory('achievementService', ['$http', 'app.config', function ($http, config) {

    var achievementServiceFactory = {};

    var _getAchievements= function () {
        return $http.get(config.serviceBase + 'api/achievement').then(function (results) {
            return results;
        });
    };

    achievementServiceFactory.getAchievements = _getAchievements;

    return achievementServiceFactory;

}]);