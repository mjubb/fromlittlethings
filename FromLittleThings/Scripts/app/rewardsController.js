﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('RewardsCtrl', ['$scope', '$location', '$window', '$rootScope', 'achievementService', function ($scope, $location, $window, $rootScope, achievementService) {
        $scope.$root.title = 'From Little Things: Animals';

        $scope.achievementModel = {};

        $scope.getAchievements = function () {
            achievementService.getAchievements().then(function (result) {
                $scope.achievementModel.achievements = result.data.achievements;
                $scope.showSpinner = false;
            });
        };

        $scope.showSpinner = true;
        $scope.getAchievements();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);