﻿'use strict';

angular.module('app.controllers')

    .controller('ResetPasswordCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.hideLoginMenu = true;
        $scope.resetPasswordModel = {};
        $scope.resetPasswordModel.email = '';
        $scope.showSpinner = false;

        $scope.resetPassword = function () {
            $scope.showError = false;
            $scope.showSuccess = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.resetPasswordModel);
            authService.resetPassword(json).then(function (response) {
                $scope.showSpinner = false;
                $scope.showSuccess = true;
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.showSuccess = false;
                 $scope.errorMessage = err.error_description || err.ExceptionMessage || 'An unknown error occured.';
             });
        };
    }]);