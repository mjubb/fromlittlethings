﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('LeaderboardsCtrl', ['$scope', '$location', '$window', '$rootScope', 'leaderboardService', function ($scope, $location, $window, $rootScope, leaderboardService) {
        $scope.$root.title = 'From Little Things: Leaderboards';

        $scope.leaderboardModel = {};

        $scope.getLeaderboards = function () {
            leaderboardService.getLeaderboards().then(function (result) {
                $scope.leaderboardModel.leaderboard = result.data.leaderboard;
                $scope.showSpinner = false;
            });
        };

        $scope.showSpinner = true;
        $scope.getLeaderboards();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);