﻿'use strict';

app.directive('gracefulImage', [function () {
    return {
        restrict: 'E',
        scope: {
            srcs: '='
        },
        template: '<img  />',
        replace: true,
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            
            scope.index = 0;
            scope.urls = [];
            scope.$watch('srcs', function () {
                if (scope.srcs && scope.srcs.length) {
                    attrs.$set('src', scope.srcs[scope.index]);                    
                }
            });

            elem.bind('error', function () {
                if (scope.index <= scope.srcs.length) {
                    scope.index++;
                    attrs.$set('src', scope.srcs[scope.index]);
                }
            });
        }
    }
}]);