﻿'use strict';
app.factory('commentService', ['$http', 'app.config', function ($http, config) {

    var commentServiceFactory = {};

    var _createComment = function (itemId, text) {

        var data = {
            itemId: itemId,
            text: text
        };

        return $http.post(config.serviceBase + 'api/timeline/CreateComment', data).then(function (results) {
            return results;
        });
    };

    commentServiceFactory.createComment = _createComment;

    return commentServiceFactory;

}]);