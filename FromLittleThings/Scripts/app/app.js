﻿'use strict';


// Module specific configuration
angular.module('app.config', [])
  .value('app.config', {
      serviceBase: '/api/'
  });

// Declares how the application should be bootstrapped. See: http://docs.angularjs.org/guide/module
var app = angular.module('app', ['ui.router', 'app.config', 'app.filters', 'app.services', 'app.directives', 'app.controllers', 'angularMoment', 'ui.bootstrap', 'LocalStorageModule', 'ngFileUpload', 'infinite-scroll', 'pascalprecht.translate']);


    // Gets executed during the provider registrations and configuration phase. Only providers and constants can be
    // injected here. This is to prevent accidental instantiation of services before they have been fully configured.
app.config(['$stateProvider', '$locationProvider', function ($stateProvider, $locationProvider) {

        // UI States, URL Routing & Mapping. For more info see: https://github.com/angular-ui/ui-router
        // ------------------------------------------------------------------------------------------------------------

        $stateProvider
            .state('home', {
                url: '/', 
                templateUrl: '/views/index',
                controller: 'HomeCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { toAbout($q, $state, $timeout, authService) }]
                }
            })
            .state('about', {
                url: '/about',
                templateUrl: '/views/about',
                controller: 'AboutCtrl'
            })
            .state('timeline', {
                url: '/timeline',
                templateUrl: '/views/timeline',
                controller: 'TimelineCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('things', {
                url: '/things',
                templateUrl: '/views/things',
                controller: 'ThingsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('leaderboards', {
                url: '/leaderboards',
                templateUrl: '/views/leaderboards',
                controller: 'LeaderboardsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('rewards', {
                url: '/rewards',
                templateUrl: '/views/rewards',
                controller: 'RewardsCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('userProfile', {
                url: '/userProfile',
                templateUrl: '/views/userProfile',
                controller: 'UserProfileCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('frieds', {
                url: '/friends',
                templateUrl: '/views/friends',
                controller: 'FriendCtrl',
                resolve: {
                    auth: ['$q', '$state', '$timeout', 'authService', function ($q, $state, $timeout, authService) { authenticate($q, $state, $timeout, authService) }]
                }
            })
            .state('signup', {
                url: '/signup',
                templateUrl: '/views/signup',
                controller: 'SignupCtrl'
            })
            .state('login', {
                url: '/login',
                templateUrl: '/views/login',
                controller: 'LoginCtrl'
            })
            .state('logout', {
                url: '/logout',
                templateUrl: '/views/about',
                controller: 'LogoutCtrl'
            })
            .state('resetPassword', {
                url: '/resetPassword',
                templateUrl: '/views/resetPassword',
                controller: 'ResetPasswordCtrl'
            })
            .state('confirmPassword', {
                url: '/confirmPassword?userId&code',
                templateUrl: '/views/confirmPassword',
                controller: 'ConfirmPasswordCtrl'
            })
            .state('otherwise', {
                url: '*path',
                templateUrl: '/views/404',
                controller: 'Error404Ctrl'
            });


        function toAbout($q, $state, $timeout, authService) {
            if ((authService.authentication) && (authService.authentication.isAuth)) {
                $timeout(function () {
                    $state.go('userProfile')
                });
            } else {
                $timeout(function () {
                    $state.go('about')
                });
            }
        }


        function authenticate($q, $state, $timeout, authService) {
            if ((authService.authentication) && (authService.authentication.isAuth)) {
                return $q.when(authService.authentication.isAuth);
            } else {
                $timeout(function () {
                    $state.go('login')
                });
            }
        }

        $locationProvider.html5Mode(true);

    }])

    // Gets executed after the injector is created and are used to kickstart the application. Only instances and constants
    // can be injected here. This is to prevent further system configuration during application run time.
    .run(['$templateCache', '$rootScope', '$state', '$stateParams', function ($templateCache, $rootScope, $state, $stateParams) {

        // <ui-view> contains a pre-rendered template for the current view
        // caching it will prevent a round-trip to a server at the first page load
        var view = angular.element('#ui-view');
        $templateCache.put(view.data('tmpl-url'), view.html());

        // Allows to retrieve UI Router state information from inside templates
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.$on('$stateChangeSuccess', function (event, toState) {

            // Sets the layout name, which can be used to display different layouts (header, footer etc.)
            // based on which page the user is located
            $rootScope.layout = toState.layout;
        });

    }]);


app.run(['$rootScope', 'authService', function ($rootScope, authService) {
    authService.fillAuthData();

    $rootScope.authentication = authService.authentication;

    $rootScope.$watch('authService.authentication.isAuth', function (newVal, oldVal, scope) {
        $rootScope.authentication = authService.authentication;
    })

}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});
