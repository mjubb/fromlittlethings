﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('NotificationsCtrl', ['$scope', '$location', '$window', '$rootScope', '$interval', 'notificationService', 'authService', function ($scope, $location, $window, $rootScope, $interval, notificationService, authService) {

        $scope.notifications = [];

        $scope.getNotifications = function () {

            var auth = authService.getAuthData();
        
            if (auth && auth.token) {
                notificationService.getUnreadNotifications().then(function (result) {
                    $scope.notifications = result.data.notifications;
                });
            }
            else {
                $scope.notifications = [];
            }
        };

        $scope.closeAlert = function (index) {
            if ($scope.notifications.length > index) {
                $scope.notifications.splice(index, 1);
            }
        }

        $rootScope.$on('checkNotifications', function () {
            $scope.getNotifications();
        });

        $interval(function () {
            $scope.getNotifications();
        }, 60000);

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);