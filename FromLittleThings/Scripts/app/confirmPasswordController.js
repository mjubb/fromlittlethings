﻿'use strict';

angular.module('app.controllers')

    .controller('ConfirmPasswordCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.showSuccess = false;
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.hideLoginMenu = true;
        $scope.confirmPasswordModel = {};
        $scope.confirmPasswordModel.userId = $location.search().userId || '';
        $scope.confirmPasswordModel.token = $location.search().code || '';
        $scope.confirmPasswordModel.newPassword = '';
        $scope.showSpinner = false;

        $scope.isValid = function () {
            return ($scope.confirmPasswordModel.userId != false) && ($scope.confirmPasswordModel.token != false);
        };

        $scope.confirmPassword = function () {
            $scope.showError = false;
            $scope.showSuccess = false;
            $scope.showSpinner = true;
            var json = angular.toJson($scope.confirmPasswordModel);
            authService.confirmPassword(json).then(function (response) {
                $scope.showSpinner = false;
                $scope.showSuccess = true;
                $timeout(function () {
                    $location.path("/login");
                }, 1000);
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.showSuccess = false;
                 $scope.errorMessage = err.error_description || err.Message || 'An unknown error occured.';
             });
        };
    }]);