﻿'use strict';
app.factory('activityService', ['$http', 'app.config', function ($http, config) {

    var activityServiceFactory = {};

    var _getActivities = function (asAtDate) {
        var parameter = asAtDate.toISOString();
        return $http.get(config.serviceBase + 'api/activity?asAtDate=' + parameter).then(function (results) {
            return results;
        });
    };

    var _updateActivities = function (json) {

        return $http.post(config.serviceBase + 'api/activity', json).then(function (results) {
            return results;
        });
    }

    activityServiceFactory.getActivities = _getActivities;
    activityServiceFactory.updateActivities = _updateActivities;

    return activityServiceFactory;

}]);