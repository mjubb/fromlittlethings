﻿'use strict';

app.directive('commentsDirective', ['commentService', function (commentService) {
    return {
        restrict: 'E',
        scope: {
            comments: '=',
            id: '=',
            user: '=',
        },
        templateUrl: '/Scripts/templates/comments.html',
        controller: ['$scope', '$http', function ($scope, $http) {

        }],
        link: function (scope, elem, attrs, ctrl) {
            scope.newComment = '';
            scope.showSpinner = false;
            scope.addComment = function () {
                if (scope.newComment) {
                    scope.showSpinner = true;
                    commentService.createComment(scope.id, scope.newComment).then(function (result) {
                        scope.comments.comments.push(result.data.comments[0]);
                        scope.newComment = '';
                        scope.showSpinner = false;
                    }, function(error) {
                        scope.showSpinner = false;
                    });
                }
            };
        }
    }
}]);