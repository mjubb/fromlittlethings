﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('UserProfileCtrl', ['$scope', '$location', '$window', '$rootScope', 'authService', 'userProfileService', 'Upload', function ($scope, $location, $window, $rootScope, authService, userProfileService, Upload) {
        $scope.$root.title = 'From Little Things: Profile';

        $scope.userProfileModel = {};
        $scope.userProfileModel.imageUrl = '';
        $scope.getUserProfile = function () {
            userProfileService.getUserProfile().then(function (result) {
                $scope.userProfileModel.firstName = result.data.firstName;
                $scope.userProfileModel.lastName = result.data.lastName;
                $scope.userProfileModel.imageUrl = result.data.imageUrl;
                $scope.userProfileModel.imageUrls = result.data.imageUrls;
                $scope.userProfileModel.imageThumbUrl = result.data.imageThumbUrl;
                $scope.userProfileModel.imageThumbUrls = result.data.imageThumbUrl;
                $scope.userProfileModel.points = result.data.points;
                $scope.userProfileModel.history = result.data.history;
                $scope.showSpinner = false;
            }, function(error) {
                $scope.showSpinner = false;
            });
        };

        $scope.logOut = function () {
            authService.logOut();
            $location.path('/about');
        }

        $scope.authentication = authService.authentication;

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        $scope.$watch('files', function () {
            $scope.upload($scope.files);
        });

        $scope.upload = function (files) {
            if (files && files.length) {
                $scope.showUploadSpinner = true;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    Upload.upload({
                        url: '/api/api/userProfile/UploadProfileImage',
                        file: file
                    }).progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                       // console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
                    }).success(function (data, status, headers, config) {
                        //$scope.userProfileModel.imageUrl = '//:0';
                        $scope.userProfileModel.imageUrls = data.imageUrls;
                        $scope.showUploadSpinner = false;
                        //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    }).error(function (er) {
                        $scope.showUploadSpinner = false;
                    });
                }
            }
        };

        $scope.showSpinner = true;
        $scope.getUserProfile();

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });

        
    }]);