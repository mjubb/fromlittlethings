﻿'use strict';
app.factory('timelineService', ['$http', 'app.config', function ($http, config) {


    var timelineServiceFactory = {};

    var _getTimeline = function (take, before) {
        return $http.get(config.serviceBase + 'api/timeline?take='+take+'&before='+before).then(function (results) {
            return results;
        });
    };

    timelineServiceFactory.getTimeline = _getTimeline;

    return timelineServiceFactory;

}]);