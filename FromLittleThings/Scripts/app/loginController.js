﻿'use strict';

angular.module('app.controllers')

    .controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'authService', function ($scope, $rootScope, $location, $timeout, authService) {
        $scope.loginData = {
            userName: "",
            password: ""
        };
        $scope.showError = false;
        $scope.errorMessage = '';
        $scope.message = "";
        $scope.hideLoginMenu = true;
        $scope.showSpinner = false;

        $scope.login = function () {
            $scope.showError = false;
            $scope.showSpinner = true;
            authService.login($scope.loginData).then(function (response) {
                $scope.showSpinner = false;
                $location.path('/');
            },
             function (err) {
                 $scope.showSpinner = false;
                 $scope.showError = true;
                 $scope.errorMessage = err.error_description;
             });
        };
    }]);