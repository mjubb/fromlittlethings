﻿'use strict';
app.factory('friendService', ['$http', 'app.config', function ($http, config) {

    var friendServiceFactory = {};

    var _getFriends = function () {
        return $http.get(config.serviceBase + 'api/friend').then(function (results) {
            return results;
        });
    };

    var _addFriend = function (email) {

        var data = {
            email: email
        };

        return $http.put(config.serviceBase + 'api/friend', data).then(function (results) {
            return results;
        });
    };

    var _deleteFriend = function (id) {

        return $http.delete(config.serviceBase + 'api/friend/' + id).then(function (results) {
            return results;
        });
    };

    friendServiceFactory.getFriends = _getFriends;
    friendServiceFactory.addFriend = _addFriend;
    friendServiceFactory.deleteFriend = _deleteFriend;

    return friendServiceFactory;

}]);