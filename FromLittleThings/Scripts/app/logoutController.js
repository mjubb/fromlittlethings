﻿'use strict';

angular.module('app.controllers')

    .controller('LogoutCtrl', ['$scope', '$location', '$timeout', 'authService', function ($scope, $location, $timeout, authService) {

        authService.logOut().then(function (response) {
            $location.path('/');
        });

    }]);