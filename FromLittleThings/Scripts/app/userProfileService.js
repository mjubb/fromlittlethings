﻿'use strict';
app.factory('userProfileService', ['$http', 'app.config', function ($http, config) {

    var userProfileServiceFactory = {};

    var _getUserProfile = function () {
        return $http.get(config.serviceBase + 'api/userProfile').then(function (results) {
            return results;
        });
    };

    var _uploadUserProfileImage = function (data) {
        return $http.post(config.serviceBase + 'api/userProfile/UploadProfileImage', data).then(function (results) {
            return results;
        });
    };

    userProfileServiceFactory.getUserProfile = _getUserProfile;
    userProfileServiceFactory.uploadUserProfileImage = _uploadUserProfileImage;

    return userProfileServiceFactory;

}]);