﻿'use strict';

// Google Analytics Collection APIs Reference:
// https://developers.google.com/analytics/devguides/collection/analyticsjs/

angular.module('app.controllers')
    // Path: /news
    .controller('FriendCtrl', ['$scope', '$location', '$window', '$rootScope', 'friendService', function ($scope, $location, $window, $rootScope, friendService) {
        $scope.$root.title = 'From Little Things: Timeline';

        $scope.friendModel = {};
        $scope.addModel = {};

        $scope.showSpinner = true;

        $scope.getFriends = function () {
            friendService.getFriends().then(function (result) {
                $scope.friendModel.friends = result.data.friends;
                $scope.showSpinner = false;
            }, function(e){
                $scope.showSpinner = false;
                $scope.friendModel.error = true;
            });
        };

        $scope.getFriends();

        $scope.addFriend = function () {
            $scope.addModel.error = false;
            $scope.addModel.success = false;
            $scope.showAddSpinner = true;
            friendService.addFriend($scope.addModel.email).then(function (result) {
                $scope.addModel.success = true;
                $scope.getFriends();
                $scope.showAddSpinner = false;
            }, function (error) {
                $scope.addModel.error = true;
                $scope.addModel.errorMessage = error.data.Message || 'An error occured attempting to process your request.';
                $scope.showAddSpinner = false;
            });
        }

        $scope.deleteFriend = function (id) {
            if (confirm('Are you sure you want to remove this person from your friends?')) {
                $scope.showSpinner = true;
                friendService.deleteFriend(id).then(function (result) {
                    $scope.getFriends();
                });
            }
        }

        $scope.$on('$viewContentLoaded', function () {
            $window.ga('send', 'pageview', { 'page': $location.path(), 'title': $scope.$root.title });
        });
    }]);