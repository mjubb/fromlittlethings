﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Resources;
using System.Reflection;
using System.Collections;
using System.Globalization;

namespace FromLittleThings.Resources
{
    public class WebContentResourceService : ResourceService
    {
        private ResourceManager ResourceManager { get; set; }
        
        public WebContentResourceService(string culture)
            : base(culture)
        {
            
            ResourceManager = new ResourceManager("FromLittleThings.Resources.Web.WebsiteContent", typeof(WebContentResourceService).Assembly);
        }

        protected string Get(string key)
        {
            return ResourceManager.GetString(key);
        }

        public string NavigationAchievements { get { return Get("NavigationAchievements"); } }

        public string NavigationAddActivity { get { return Get("NavigationAddActivity"); } }

        public string NavigationFriends { get { return Get("NavigationFriends"); } }

        public string NavigationLeaderboards { get { return Get("NavigationLeaderboards"); } }

        public string NavigationTimeline { get { return Get("NavigationTimeline"); } }

        public override IDictionary<string, string> GetDictionary()
        {
            var resourceSet = ResourceManager.GetResourceSet(new CultureInfo(Culture), true, true);

            var dictionary = new Dictionary<string, string>();
            var enumerator = resourceSet.GetEnumerator();
            while (enumerator.MoveNext())
            {
                dictionary.Add(enumerator.Key.ToString(), enumerator.Value.ToString());
            }
            return dictionary;

        }
    }
}
