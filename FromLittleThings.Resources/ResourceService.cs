﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromLittleThings.Resources
{
    public abstract class ResourceService
    {
        protected string Culture { get; private set; }

        public ResourceService(string culture)
        {
            Culture = culture;
        }

        public abstract IDictionary<string, string> GetDictionary();
   
    }
}
